/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Jamshed.Latipov
 */

import java.sql.SQLException;
import java.util.List;
import models.api.SellerPassword;
import org.junit.Assert;
import org.junit.Test;
import static org.mockito.Mockito.*;
public class custTest {
    
    @Test
    public void test001(){
        //mock creation
        List mockedList = mock(List.class);

        //using mock object
        mockedList.add("one");
        mockedList.clear();
        
        //verification
        verify(mockedList).add("one");
        verify(mockedList).clear();
    }
    
    @Test
    public void testSellerPasswordIsConfirmed() throws SQLException{
        SellerPassword sellerPassword = new SellerPassword();
        sellerPassword.setPswdId(1);
        sellerPassword.setSellerId(2);
        sellerPassword.setSellerPswd("123");
        sellerPassword.setSellerPswdCondidate("123");
        Assert.assertEquals(sellerPassword.checkCondidateIsConfirmed(), SellerPassword.OK);
    }
    
    
    @Test
    public void testSellerPasswordIsNotConfirmed() throws SQLException{
        SellerPassword sellerPassword = new SellerPassword();
        sellerPassword.setPswdId(1);
        sellerPassword.setSellerId(2);
        sellerPassword.setSellerPswd("123");
        sellerPassword.setSellerPswdCondidate("123");
        Assert.assertEquals(sellerPassword.checkCondidateIsNotConfirmed(), SellerPassword.OK);
    }
    
    
    
}
