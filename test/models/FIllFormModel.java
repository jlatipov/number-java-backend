package models;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 *
 * @author jamshedlatipov
 */

public class FIllFormModel implements impl.implFillFormModel{

    @Override
    public int insert() throws SQLException, IOException, ParseException {
        return 0;
    }

    @Override
    public String bpRegister(int order_id) throws SQLException {
        return "";
    }

    @Override
    public boolean changeState(int id) {
        return true;
    }

    @Override
    public boolean uploadImage(String type, int orderId) {
        return true;
    }
    
}
