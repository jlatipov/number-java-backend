<%-- 
    Document   : login
    Created on : Nov 19, 2015, 10:29:22 AM
    Author     : Jamshed.Latipov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="includes/headContaint.jsp"/>
    </head>
    <body>
        <%session.setAttribute("title","Мархамат намоед!");%>
        
        <jsp:include page="includes/menu.jsp"/>
          <div class = 'container'>
                <form action ="/choose/Auth" method = 'post' id = 'fillFormId'>
                    <div class="form-group" >
                        <input name ='login' class="form-control input-lg" type = 'text' placeholder="Логин">
                    </div>
                    <div class="form-group">
                        <input name ='password' type ="password" class="form-control input-lg widthMedium flat" placeholder="Рамз" >
                    </div>
                    
                    <div class="alert-danger" style ='display: none' id = 'stateBlock'>
                        Пожалуйста заполните обведенные красным поля
                    </div>
                    
                    <div class="form-group">
                        <input type ="submit" class="form-control input-lg widthMedium flat pink"  value="Вуруд" >
                    </div>
                </form>
            </div>
    </body>
    <script>
        var form = document.forms[0];
        var inputs = form.getElementsByTagName("input");
        
        var login = inputs[0];
        var password = inputs[1];
        var button = inputs[2];
       
        button.addEventListener("click", function(e){
            var passValid = checkIsFilled(password);
            var loginValid = checkIsFilled(login);
            if(passValid || loginValid){
                e.preventDefault();
            }
        },false);
        
        function checkIsFilled(elem){
            resetStyling(elem);
            var elemValid = elem.value.length >= 1;
            if(!elemValid){
                elem.style.border = "3px solid red";
                document.getElementById("stateBlock").style.display = "block";
                return true;
            }
            return false;
        }
        
        function resetStyling(elem){
            elem.style.border = "3px solid lightgray";
        }
    </script>
    
</html>
