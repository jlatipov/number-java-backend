<%-- 
    Document   : test
    Created on : Nov 12, 2015, 6:11:42 PM
    Author     : Jamshed.Latipov
--%>
<%@page import="utils.Hash"%>
<%@page import="utils.SMS"%>
<%@page import="people.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="NumberRelation.Category"%>
<%@page import="NumberRelation.Number"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    boolean requestIsPost = request.getMethod().equals("POST");
    
    String paramPattern = "";
    String paramCategory = "0";
    
    if(requestIsPost){
        paramPattern = request.getParameter("number");
        paramCategory = request.getParameter("category");
    }
    
    User usr = new User();
    utils.Hash hash = new Hash();
    
    String password = usr.generateString(6);
    hash.md5Custom(password);
    
    SMS sms = new SMS();
    sms.setTo("992928349009");
    
    sms.setMsg("Ваш пароль: " + password +", от dealer.tcell.tj.");
    sms.sendSMS();
     
%>

<!DOCTYPE html><!DOCTYPE html>
<html lang="en">
  <head>
      <jsp:include page="includes/headContaint.jsp"/>
  </head>

  <body>
      <% session.setAttribute("title", "Интихоби ракам"); %>
    <jsp:include page="includes/menu.jsp"/>

    <div class="container">
        <form action = '' method='post'>
            <div class="form-group">
                <input name = 'number' type="number" class="form-control input-lg" placeholder="Ракам" value ='<%= paramPattern %>'>
            </div>
            <div class="form-group">
                <select name = 'category'  class="form-control input-lg">
                    <% Category category = new Category(); %>
                    <%= category.generateSelectOptions(category, paramCategory) %>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" onclick="numberSearchButtonClick(this)" class = "form-control input-lg span11 pink" id = 'numberSearchButton'>Чустучу</button>
            </div>
        </form>
        <div>
            <% if(requestIsPost) {%>
                <% 
                    Number number = new Number(); 
                    ArrayList<Number> numbers = number.getNumbersList(paramCategory, paramPattern);
                    
                %>
                <%= numbers%>
                <div  class = 'row'>
                    < % if(numbers.size() > 0){ %>
                        < % for(Number n : numbers){%>
                            <div class="col-lg-3 col-sm-6 col-xs-6 col-md-4">
                                <a class = 'btn myLabel' href = 'order.jsp?msisdn_id=< %=n.getId()%>'>
                                    < %= n.getShortNumber()%>
                                </a>
                            </div><!-- /.col-lg-6 -->
                        < %}%>
                    < % }else{%>
                        <div class='col-lg-12 col-sm-12 col-xs-12 col-md-12 text-center alert-danger'>ракам мавчуд нест</div>
                    < %}%>
                </div>
            <%}%>
        </div>
<!--        <div>
            < % if(session.getAttribute("userFullName") != null){ %>
                Здравствуйте, <b> < %=session.getAttribute("userFullName").toString() %> </b>!
            < %}else{response.sendRedirect("login.jsp");}%>
        </div>-->
        
    </div> <!-- /container -->
    
    <script>
        function numberSearchButtonClick(button){
            button.style.disabled = true;
            button.innerHTML = "Интизор шавед...";
            button.style.background = "gray";
        }
    </script>
  </body>
</html>