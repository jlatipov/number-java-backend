<%-- 
    Document   : menu
    Created on : Nov 13, 2015, 12:13:39 AM
    Author     : Jamshed.Latipov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
        <a class="navbar-brand pull-left" href="#"><%=session.getAttribute("title")%></a>
    </div>
  </div>
</nav>
