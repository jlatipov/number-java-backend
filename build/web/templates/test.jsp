<%-- 
    Document   : Order
    Created on : Nov 10, 2015, 8:41:18 AM
    Author     : Jamshed.Latipov
--%>

<%@page import="NumberRelation.Number"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    int msisdnId = Integer.parseInt(request.getParameter("msisdn_id"));
%>
<!DOCTYPE html>


<html>
    <head>
        <link href = '../static/style.css' rel='stylesheet' type='text/css'></link>
        <script src = '../static/script.js' ></script>
    </head>
    <body>
        <div id = 'main' class = 'widthMedium'>
            <div id = 'header' class = 'purple'>Поиск номера</div>
            <div id = 'container' class ='widthLarge marginTop' >
                <% 
                    Number number = new Number(); 
                    number.load(msisdnId);
                %>
                
                <div id = "numberPreview">
                    <div class ='simLargeImg' src = '' style='border:1px solid'>
                        IMG
                    </div>
                    <div class ='simProperty'>
                        Выбранный номер: <b><%=number.getShortNumber()%></b>
                    </div>
                    
                    <div class ='simProperty'>
                        Категория: <b><%=number.getCategory()%></b>
                    </div>
                    
                    <div class ='simProperty'>
                        Цена за категорию: <b><%=number.getPrice()%></b>
                    </div>
                </div>
                <div class = 'marginTop buttonContainer'>
                    <a href ='fillForm.jsp?msisdn_id=<%=msisdnId%>'>
                        <div class = 'green flat btn '>Оформить покупку</div>
                    </a>                    
                    <a href ='/choose' ><div class = 'pink flat btn '>Назад</div></a>

                </div>
            </div>
        </div>
    </body>
</html>