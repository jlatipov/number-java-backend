<%-- 
    Document   : Order
    Created on : Nov 10, 2015, 8:41:18 AM
    Author     : Jamshed.Latipov
--%>

<%@page import="NumberRelation.Number"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    int msisdnId = Integer.parseInt(request.getParameter("msisdn_id"));
    
    Number number = new Number(); 
    number.load(msisdnId);
    
    
   
%>
<!DOCTYPE html>


<html lang="en">
    <head>
        <jsp:include page="includes/headContaint.jsp"/>
    </head>

    <body>
        <% session.setAttribute("title", "Маълумот оиди ракам"); %>
        <jsp:include page="includes/menu.jsp"/>

        <div class="container">
            <div class="media myDescription">
                <div class="media-left col-sm-3 col-xs-3 col-md-3 col-lg-3 ">
                    <a href="#">
                      <img class="media-object" src="..." alt="..." style='border:1px solid'>
                    </a>
                </div>
                <div class="media-body">
                    <div class ='simProperty'>
                        Раками интихобшуда: <b><%=number.getShortNumber()%></b>
                    </div>

                    <div class ='simProperty'>
                        Категория: <b><%=number.getCategory()%></b>
                    </div>

                    <div class ='simProperty'>
                        Арзиши ракам: <b><%=number.getPrice()%></b>
                    </div>
                </div>
            </div>
            <div class = 'form-group'>
                <a class =' input-lg green col-md-7 col-xs-7 col-lg-7 col-sm-7' href ='fillForm.jsp?msisdn_id=<%=msisdnId%>'>бакайдгири</a>
                <a class="input-lg pink col-md-4 col-xs-4 col-lg-4 col-sm-4  col-xs-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-offset-1" href ='/choose/templates'>бозгашт</a>
                
            </div>
        </div>
    </body>
</html>