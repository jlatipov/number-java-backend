/* global autoCompleteContainer */

function AJAX(){
    
    this.getXmlHttp = function(){
        var xmlhttp;
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        return xmlhttp;
    };
    
    this.send = function(url,callBack){
        var req = this.getXmlHttp();
        req.onreadystatechange = function() {
            if (req.readyState === 4) {
               if(req.status === 200) {
                   if(callBack !== undefined && typeof callBack === 'function'){
                        callBack(req.responseText);
                    }else{
                        return req.responseText;
                    }
                }
            }
        };
        req.open('GET', url, true);
        req.send(null);
    };
} 

var ajax = new AJAX(); 

function initAutocomplete(that){
    var parent = that.parentNode;
    var autoCompleteContainer = parent.getElementsByClassName("autocomplete")[0];
    
    
    
    var send = function(){
        ajax.send("/choose/Autocomplete?pattern="+encodeURIComponent(that.value), function(response){    
            drawAutoComlpete(autoCompleteContainer,response);
        });
    };


    send();
    that.addEventListener('keyup',function(e){
        send();
    },false);

    that.addEventListener('blur',function(e){
        setTimeout(function(){
            autoCompleteContainer.style.display = 'none';
        },100);

    },false);
}


function drawAutoComlpete(element,response){
    element.style.display = 'block';
    element.innerHTML = response;
}


function autocompleteGetObject(that){
    var parent = that.parentNode.parentNode;
    var input = parent.getElementsByTagName("input")[0];
    input.value = that.childNodes[0].innerHTML;
    parent.getElementsByClassName("valueContainer")[0].value = that.getAttribute("data-id");
    
    return false;
}



 function getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;

    while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}