function Application(){
    this.maskNumber = function(inp){
        var result = '';
        var number = inp.value;
        var len = number.length;
        var maskedNumber = [];
        maskedNumber[0] = number.substr(0,2) || false;
        maskedNumber[1] = number.substr(2,3) || false; 
        maskedNumber[2] = number.substr(3,4) || false; 
        maskedNumber[3] = number.substr(4,5) || false; 
        var concat = "";
        for(var i = 0;i<=3;i++){
                concat = '-' + maskedNumber[i];
                if(maskedNumber[i]) 
                        result += concat;
                else
                        break;
        }
        console.log(result);
    }    
}

function User(){
    this.fullName = "";
    this.gender = 0;
    this.passport_serial = "";
    this.passport_get_place = "";
    this.city = 0;
    this.street = "";
    this.address = "";
    this.born_date = "";
    
    this.setCookies = function(){
        document.cookie = "fullName="+this.fullName;
        document.cookie = "gender=" + this.gender;
        document.cookie = "passport_serial=" + this.passport_serial;
        document.cookie = "passport_get_place=" + this.passport_get_place;
        document.cookie = "city="+this.city;
        document.cookie = "street="+this.street;
        document.cookie = "address="+this.address;
        document.cookie = "born_date="+this.born_date;
    }    
}

var app = new Application();
var validate = app.maskNumber;

