/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Jamshed Latipov
 */
public class SQL {
    
    public static final int DEALER_ID_FOR_CHI_GAP_MSISDN_SELECT = 974;
    
    
    
    public static final String GET_CHI_GAP_MSISDN 
            = 
            " SELECT * " +
                " FROM (SELECT ROWNUM rnum, lst.* " +
                    " FROM ("
                            + " select " +
                                " ph.phone_id  phone_id" +
                                ", u.branch_id branch_id" +
                                ", ph.msisdn msisdn" +
                                ", pt.phone_type phone_type" +
                            " from party p, " +
                                " party_details pd, " +
                                " usi u, " +
                                " phone_type pt, "+
                                " usi_history uh, " +
                                " subs_usi_history suh, " +
                                " subscriber s, " +
                                " subs_history sh, " +
                                " phone ph " +
                                ", phone_history phh " +
                            " where p.dealer_id= " + DEALER_ID_FOR_CHI_GAP_MSISDN_SELECT +
                                " and p.PARTY_ID=pd.PARTY_ID " +
                                " and u.PARTY_DETAIL_ID = pd.party_detail_id " +
                                " and u.usi_id=suh.usi_id " +
                                " and sysdate between suh.stime and suh.etime-1/86400 " +
                                " and u.usi_id=uh.usi_id " +
                                " and sysdate between uh.stime and uh.etime-1/86400 " +
                                " and suh.subs_id=s.subs_id " +
                                " and u.usi_status_id = 4 " +
                                " and s.activation_date is null " +
                                " and suh.subs_id=sh.subs_id " +
                                " and sysdate between sh.stime and sh.etime-1/86400 " +
                                " and sh.phone_id=ph.phone_id " +
                                " and sh.stat_id=1 " +
                                " and phh.phone_id = ph.phone_id " +
                                " and phh.pht_id = pt.pht_id " +
                                " and ph.msisdn like ? " +
                                " and sysdate BETWEEN phh.stime and phh.etime " +
                            " ) lst "+
                " WHERE ROWNUM <= (? + 10)) " + //3 page
            " WHERE rnum > ?";
    
    public static final String GET_BALANCE 
            = "SELECT "
                + "cb.balance_$ bal "
            + "FROM phone p"
                + ", subs_history sh"
                + ", client_balance cb "
            + "WHERE "
                + "p.msisdn = to_char(?) "
                + "AND p.phone_id = sh.phone_id "
                + "AND cb.clnt_id = sh.clnt_id "
                + "AND SYSDATE BETWEEN sh.stime AND sh.etime "
                + "AND cb.balance_id=0";
    
    public static String UPDATE_IMAGE_FRONT 
            = "update cch.number_order set pass_img = ? where id = ?";
    
    
    public static String CHANGE_ORDER_STATUS 
            = "update cch.number_order set status = 999 where id = ?";
    
    
    public static String UPDATE_IMAGE_BACK 
            = "update cch.number_order set pass_img_back = ? where id = ?";
    
    public static final String INSERT_ORDER 
        = "begin "
            + "INSERT INTO cch.number_order ("
                + " name "
                + ", middle_name "
                + ", last_name "
                + ", icc "
                + ", gender "
                + ", born_date "
                + ", cre_date "
                + ", status "
                + ", wd_order_id "
                + ", msisdn "
                + ", adr_city "
                + ", addr_country "
                + ", adr_jamoat "
                + ", adr_kishlak "
                + ", adr_rayon "
                + ", adr_region "
                + ", adr_street "
                + ", adr_house "
                + ", pass_number "
                + ", pass_series "
                + ", pass_when "
                + ", pass_who "
                + ", dealer_id "
                + ", seller_id "
                + ", tariff_plan_id"
                + ", contact_person"
                + ", order_type"
                + ", client_ip"
                + ", pass_type"
                + ", email"
            + ") "
            + "VALUES ("
                + " ?" // name 1
                + ", ?" // middle_name 2
                + ", ?" // last_name 3
                + ", ?" // icc 4
                + ", ?" // gender 5
                + ", to_date(?, 'dd-mm-yyyy')" // born_date 6
                + ", sysdate" // cre_date 
                + ", 0" // status 
                + ", 0" // wd_order_id 
                + ", ?" // msisdn 7
                + ", ?" // adr_city 8
                + ", ?" // addr_country 9
                + ", ?" // adr_jamoat 10
                + ", ?" // adr_kishlak 11
                + ", ?" // adr_rayon 12
                + ", ?" // adr_region 13
                + ", ?" // adr_street 14
                + ", ?" // adr_house 15
                + ", ?" // pass_number 16
                + ", ?" // pass_series 17
                + ", to_date(?, 'dd-mm-yyyy')" // pass_when 18
                + ", ?" // pass_who 19
                + ", ?" // dealer_id 20
                + ", ?" // sellerid 21
                + ", ?" // trplid 22
                + ", ?" // contact_person 23
                + ", ?" // order_type 24
                + ", ?" // client_ip 25
                + ", ?" // pass_type 26
                + ", ?" // email 27


            + ") returning id into ?; "
        + "end;";
    
    public static final String BP_REGISTER 
            = "begin "
                + " wd_numselect.register_order("
                    + "? " //":i_order_id"
                    + ",? " //", :o_wd_order_id"
                    + ",? "//", :o_err_code"
                    + ",? " //", :o_err_msg"
                + "); "
            + "end; ";
    
    public static final String GET_MSISDN_BY_ID 
            = "select "
                + "fpl.phone_id phone_id"
                + ", fpl.branch_id branch_id"
                + ", fpl.mask mask"
                + ", fpl.msisdn msisdn"
                + ", pt.phone_type phone_type  "
            + "from "
                + "free_phones_list fpl"
                + ", phone_type pt "
            + "where "
                + "fpl.pht_id = pt.pht_id "
                + " and fpl.phs_id = 1 " 
                + " and fpl.phc_id <> 9 " 
                + " and fpl.pht_id <> 10 " 
                + "and phone_id = ?";
    
    
    public static final String GET_MSISDN_LIST_WITHOUT_PHONE_TYPE 
        = "SELECT * " +
            "FROM (SELECT ROWNUM rnum, lst.* " +
                "FROM (SELECT fpl.phone_id, fpl.branch_id, fpl.mask, fpl.msisdn, pt.phone_type " +
                    "FROM  "+
                       " free_phones_list fpl, phone_type pt" +
                    " WHERE "+
                       " reverse (fpl.msisdn) LIKE ? " + // 1 MSISDN 
                       " and fpl.branch_id = ? " + // 2 Branch_ID
                       " and fpl.phs_id = 1 " +
                       " and fpl.phc_id <> 9 " +
                       " and fpl.pht_id <> 10 " +
                       " and fpl.pht_id = pt.pht_id " +
                 " ) lst "+
            " WHERE ROWNUM <= (? + 10)) " + //3 page
        " WHERE rnum > ?";
    
    public static final String GET_MSISDN_LIST_WITH_PHONE_TYPE 
        = "SELECT * " +
            "FROM (SELECT ROWNUM rnum, lst.* " +
                  "FROM (SELECT fpl.phone_id, fpl.branch_id, fpl.mask, fpl.msisdn, pt.phone_type " +
                           "FROM  "+
                              " free_phones_list fpl, phone_type pt" +
                           " WHERE "+
                              " reverse (fpl.msisdn) LIKE ? " + // 1 MSISDN 
                              " and fpl.branch_id = ? " + // 2 Branch_ID
                              " and fpl.phs_id = 1 " +
                              " and fpl.phc_id <> 9 " +
                              " and fpl.pht_id <> 10 " +
                              " and fpl.pht_id = pt.pht_id " + // 3 category
                              " and fpl.pht_id = ? " + // 3 category
                        " ) lst "+
                  " WHERE ROWNUM <= (? + 10)) " + //4 which page 
        " WHERE rnum > ?"; // 5 page 
    
    public static final String GET_PHONE_TYPE 
        = "select * from phone_type where pht_id in(1, 7, 2, 3, 14)";
    public static final String GET_PHONE_TYPE_BY_ID 
            = "select * from phone_type where pht_id = ?";
    
    public static final String GET_REPORT_BY_SELLER_ID 
        = "select * "
            + "from "
                + "number_order "
            + "where "
                + " seller_id = ? "
                + " and cre_date >= sysdate - 30"
                + " order by cre_date DESC";
    
    public static final String GET_REPORT_BY_SELLER_ID_AND_MSISDN
            = "select * "
                + "from "
                    + "v_number_orders "
                + "where "
                    + " seller_id = ? "
                    + " and msisdn = ?"
                    //+ " and cre_date >= sysdate - 300"
                    + " order by cre_date DESC";
    
    public static final String GET_REPORT_BY_ORDER_ID
            = "select * "
                + "from "
                    + " NUMBER_ORDER "
                + " where "
                    + " id = ?";
    
    public static final String GET_REPORT_BY_SELLER_ID_AND_MSISDN_AND_DATE
            ="select * "
                + "from "
                    + "v_number_orders "
                + "where "
                    + " seller_id = ? "
                    + " and msisdn = ?"
                    + " and cre_date between ? and ?"
                    + " order by cre_date DESC";
    
    
    public static final String GET_SELLER_BY_ID 
            = "SELECT " +
                    "    bas.status status"+
                    "    ,bas.SELLER_ID SELLER_ID" +
                    "    ,bas.SALE_POINT_STR_ID SALE_POINT_STR_ID" +
                    "    ,bas.ADDRESS ADDRESS " +
                    "    ,bas.seller_inv_msisdn msisdn " +
                    "    ,bas.SELLER_NAME SELLER_NAME" +
                    "    ,bas.SELLER_CONTACT_PHN SELLER_CONTACT_PHN" +
                    "    ,bas.SP_CATEGORY SP_CATEGORY" +
                    "    ,bas.SP_OWNER_PHONE SP_OWNER_PHONE" +
                    "    ,bas.SP_SQUERE SP_SQUERE" +
                    "    ,bas.REGISTRATION_DATE REGISTRATION_DATE " +
                    "    ,bas.DEALER_ID DEALER_ID" +
                    "    ,bas.REGION_ID REGION_ID" +
                    " FROM " +
                    "    cch.back_administration_seller bas" +
                    " where " + 
                    "   bas.seller_id = ? ";
    
    public static final String GET_SELLER_BY_IMSI 
        = "SELECT  " +
            "p.msisdn msisdn  " +
            ",bas.status status " +
            ",bas.SELLER_ID SELLER_ID " +
            ",bas.SALE_POINT_STR_ID SALE_POINT_STR_ID " +
            ",bas.ADDRESS ADDRESS  " +
            ",bas.SELLER_NAME SELLER_NAME " +
            ",bas.SELLER_CONTACT_PHN SELLER_CONTACT_PHN " +
            ",bas.SP_CATEGORY SP_CATEGORY " +
            ",bas.SP_OWNER_PHONE SP_OWNER_PHONE " +
            ",bas.SP_SQUERE SP_SQUERE " +
            ",bas.REGISTRATION_DATE REGISTRATION_DATE " +
            ",bas.DEALER_ID DEALER_ID " +
            ",bas.REGION_ID REGION_ID " +
            ",u.branch_id branch_id " +
        "FROM  " +
            "subs_usi_history suh " +
            ", usi u " +
            ", subs_history sh " +
            ", phone p " +
            ", subscriber s " +
            ", cch.back_administration_seller bas " +
        "where  " +
            "SYSDATE BETWEEN sh.stime AND sh.etime " +
            "and SYSDATE BETWEEN suh.stime AND suh.etime " +
            "AND suh.usi_id = u.usi_id  " +
            "AND suh.subs_id = s.subs_id  " +
            "AND suh.subs_id = sh.subs_id " +
            "AND sh.phone_id=p.phone_Id " +
            "and bas.seller_inv_msisdn = p.msisdn " +
            "and u.usi = ?";
    
    public static final String GET_SELLER_BY_MSISDN 
        = "SELECT  " +
            "p.msisdn msisdn  " +
            ",bas.status status " +
            ",bas.SELLER_ID SELLER_ID " +
            ",bas.SALE_POINT_STR_ID SALE_POINT_STR_ID " +
            ",bas.ADDRESS ADDRESS  " +
            ",bas.SELLER_NAME SELLER_NAME " +
            ",bas.SELLER_CONTACT_PHN SELLER_CONTACT_PHN " +
            ",bas.SP_CATEGORY SP_CATEGORY " +
            ",bas.SP_OWNER_PHONE SP_OWNER_PHONE " +
            ",bas.SP_SQUERE SP_SQUERE " +
            ",bas.REGISTRATION_DATE REGISTRATION_DATE " +
            ",bas.DEALER_ID DEALER_ID " +
            ",bas.REGION_ID REGION_ID " +
            ",ph.branch_id branch_id " +
        "FROM  " +
            "phone p " +
            ", phone_history ph" +
            ", cch.back_administration_seller bas " +
        "where  " +
            "p.phone_id = ph.phone_id " + 
            "and sysdate BETWEEN ph.stime and ph.etime " +
            "and bas.seller_inv_msisdn = p.msisdn " +
            "and p.msisdn = ?";
    

    public static final String INSERT_SELLER_PASSWORD 
        = "insert into "
            + "cch.back_administration_salepa72af("
                + "seller_pswd"
                + ", stime"
                + ", etime"
                + ", status"
                + ", seller_id"
                + ", seller_pswd_condidat"
                + ", seller_pswd_condidat_status) "
            + "values("
                + "?"
                + ", sysdate"
                + ", (sysdate + 100)"
                + ", ?"
                + ", ?"
                + ", ?"
                + ",?)";
    
    public static final String UPDATE_SELLER_PASSWORD 
        = "update cch.back_administration_salepa72af "
            + "set SELLER_PSWD_CONDIDAT = ?"
            + ", SELLER_PSWD_CONDIDAT_STATUS = ?"
            + ", seller_pswd = ?"
            + ", status = ?  "
        + "where "
            + "pswd_id = ?";

    public static final String GET_SELLER_PASSWORD_BY_SELLER_ID 
        ="select * from cch.back_administration_salepa72af where seller_id = ?";
    
    public static final String GET_TARIFF_PLANS_RULE_WHEN_NULL 
            = "select "
                + "str.tariff_plan trpl "
            + "from "
                + "cch.sale_tariffplansrues str "
            + "where "
                + "region_id is null";
    
    public static final String GET_TARIFF_PLANS_RULE_WHEN_NOT_NULL
            = "select "
                + "str.tariff_plan trpl "
            + "from "
                + "cch.sale_tariffplansrues str "
            + "where "
                + "region_id = (select "
                        + "region_id "
                    + "from "
                        + "back_administration_seller "
                    + "where "
                        + "(seller_id = ?))";
    
    public static final String GET_MSISDN_BRANCH 
            = "select ph.branch_id branch, ph.switch_id switch"
                + " from"
                    + " phone p"
                    + ", phone_history ph "
                + "where "
                    + "p.phone_id = ph.phone_id "
                    + "and p.msisdn = ? "
                    + "and sysdate between ph.stime and ph.etime";
    
    
    public static final String GET_MSISDN_ICC 
            = "SELECT " +
                        "uh.icc icc, " +
                        "p.dealer_id dealer " +
                    "FROM usi_phone UP, " +
                        "usi uh, " +
                        "party p, " +
                        "party_details pd, " +
                        "switch s, " +
                        "dealer d, " +
                        "phone_history ph " +
                    "WHERE     p.party_id = pd.party_id " +
                        "AND uh.party_detail_id = pd.party_detail_id " +
                        "AND UP.usi_id = uh.usi_id " +
                        "AND ph.phone_id = UP.phone_id " +
                        "AND SYSDATE BETWEEN ph.stime AND ph.etime " +
                        "AND p.dealer_id = d.dlr_id " +
                        "AND ph.pht_id = 10 " +
                        "and ph.phs_id=2 " +
                        "AND s.switch_id = uh.switch_id " +
                        "and uh.branch_id = ? " +
                        "and reverse(uh.icc) like ? " +
                        "and rownum <= 10";
    
    
    public static final String GET_MSISDN_DEALER 
            = "SELECT " +
                        "uh.icc icc, " +
                        "p.dealer_id dealer " +
                    "FROM usi_phone UP, " +
                        "usi uh, " +
                        "party p, " +
                        "party_details pd, " +
                        "switch s, " +
                        "dealer d, " +
                        "phone_history ph " +
                    "WHERE     p.party_id = pd.party_id " +
                        "AND uh.party_detail_id = pd.party_detail_id " +
                        "AND UP.usi_id = uh.usi_id " +
                        "AND ph.phone_id = UP.phone_id " +
                        "AND SYSDATE BETWEEN ph.stime AND ph.etime " +
                        "AND p.dealer_id = d.dlr_id " +
                        "AND ph.pht_id = 10 " +
                        "and ph.phs_id=2 " +
                        "AND s.switch_id = uh.switch_id " +
                        "AND ph.switch_id = uh.switch_id " +
                        "and uh.branch_id = ? " +
                        "and uh.switch_id = ? " +
                        "and reverse(uh.icc) like ? " +
                        "and rownum <= 10";
    
}
