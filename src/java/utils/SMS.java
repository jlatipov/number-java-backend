/*
    Usage example

    SMS sms = new SMS();
    sms.setTo("992928349009");
    
    sms.setMsg("hello from number choose programm");
    sms.sendSMS();


 */
package utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Jamshed.Latipov
 */
public class SMS {
    private String from;
    private String to;
    private String msg;
    private int priority;
    private int result;
    
    
    public SMS(String to, String message){
        this.from = "Tcell";
        this.priority = 90;
        this.msg = message;
        this.to = to;
    }
    
    public boolean sendSMS() throws SQLException{
        DB db = new DB();
        
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        
//        smaster.send_sms_local(:i_sender, :i_msisdn, :i_msg_txt, :i_priority, :i_expire_date, :o_result);
        
        String selectSQL = "begin smaster.send_sms_local(?, ?, ?, ?, sysdate + 1 / 24, ?); end;";
        try {
                dbConnection = db.getDBConnection();
                CallableStatement callableStatement = dbConnection.prepareCall(selectSQL);
                callableStatement.setString(1, this.getFrom());
                callableStatement.setString(2, this.getTo());
                callableStatement.setString(3, this.getMsg());
                callableStatement.setInt(4, this.getPriority());
                callableStatement.registerOutParameter(5, java.sql.Types.INTEGER);
                
                callableStatement.executeUpdate();
                int result = callableStatement.getInt(5);
                
                System.out.println(result);

                if(result > 0)
                    return true;
                    
                else
                    return false;
                
                
        } catch (SQLException e) {

                System.out.println(e.getMessage());

        } finally {

                if (preparedStatement != null) {
                        preparedStatement.close();
                }

                if (dbConnection != null) {
//                        dbConnection.close();
                }

        }        
        return false;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @return the result
     */
    public int getResult() {
        return result;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @param result the result to set
     */
    public void setResult(int result) {
        this.result = result;
    }
    
    
}



