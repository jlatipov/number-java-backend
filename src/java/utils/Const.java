/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Jamshed.Latipov
 */
public class Const {
    public static final String HEADER_JSON = "application/json;charset=UTF-8";
    public static final String HTTP_METHOD_POST = "POST";
    public static final String HTTP_METHOD_GET = "GET";
    
    public static final int DB_CANNOT_FIND_RECORD = 0;
    public static final String DB_CANNOT_FIND_RECORD_MESSAGE = "Cannot find record";
    public static final String OK_MESSAGE = "OK";
    public static final int FORBIDDAN = 403;
    public static final String FORBIDDAN_MSG = "Access FORBIDDAN!";

    public static final int SMS_NOT_SEND = -1;
    public static final int SMS_SEND = 2;
    public static final String SMS_NOT_SEND_MESSAGE = "Cannot send message, please try again";    
    
    // SELLER_PASSWORD \\
    public static final int SELLER_PASSWORD_WAS_GENERATED = 1;
    public static final int SELLER_PASSWORD_SEND_GENERATION_FAILD = 0;
    
    public static final int SELLER_PASSWORD_NOT_CONFIRMED = 0;
    public static final int SELLER_PASSWORD_WAS_CONFIRMED = 1;
    public static final int SELLER_PASSWORD_ALREDY_IN_USE = 2;
    public static final int SELLER_WAS_LOCKED = 0;


    
    public static final int INTEGER_EMPTY_OR_NOT = 0;
    
    
    public static final int SELLER_DOES_NOT_EXIST = 10;
    
    public static final int OK = 1;
    

}
