/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author jamshed.latipov
 */
public class HelperModels {
    public static class DealerAndIcc{
        private int dealer;
        private int state;
        private String icc;

        /**
         * @return the dealer
         */
        public int getDealer() {
            return dealer;
        }

        /**
         * @param dealer the dealer to set
         */
        public void setDealer(int dealer) {
            this.dealer = dealer;
        }

        /**
         * @return the icc
         */
        public String getIcc() {
            return icc;
        }

        /**
         * @param icc the icc to set
         */
        public void setIcc(String icc) {
            this.icc = icc;
        }

        /**
         * @return the state
         */
        public int getState() {
            return state;
        }

        /**
         * @param state the state to set
         */
        public void setState(int state) {
            this.state = state;
        }
    }
    
}
