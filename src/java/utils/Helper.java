/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.google.gson.Gson;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import models.api.PinCode;
import models.api.SellerPassword;

/**
 *
 * @author Jamshed.Latipov
 */
public class Helper {
    
    
    
    public static String convertToBinary(InputStream image) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte buf[] = new byte[8192];
        int qt = 0;
        while ((qt = image.read(buf)) != -1) {
          baos.write(buf, 0, qt);
        }
        return baos.toString();
    }
    
    
    public static java.sql.Date getCurrentDate() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Date(today.getTime());
    }
    
    // TODO make testable 
    public static float getNumberPrice(String msisdn) throws SQLException{
        String sql = "select smaster.get_msisdn_price(?) price from dual";
        Connection dbConnection = DB.getDBConnection();
        PreparedStatement pstmt  = dbConnection.prepareStatement(sql);
        try{
            pstmt.setString(1, msisdn);

            ResultSet rs = pstmt.executeQuery();
            try{
                if(rs.next()){
                    return rs.getFloat("price");
                }
            }finally{
                rs.close();
            }
        }finally{
            pstmt.close();
        }
        return Const.DB_CANNOT_FIND_RECORD;
        
    }
    
    public static PinCode auth(String pinCode, int sellerId) throws SQLException{
        
        // finding sellerPassword
        SellerPassword sellerPassword = new SellerPassword();
        sellerPassword.find(sellerId);
        PinCode pinCodeInst = new PinCode();
        
        pinCodeInst.setState(SellerPassword.PASSWORD_NOT_EQUAL);
        
        
        if(sellerPassword.checkCondidateIsNotConfirmed() == SellerPassword.OK){
            pinCodeInst.setState(sellerPassword.authUsingCondidatePassword(pinCode));
            pinCodeInst.setHashedPassword(sellerPassword.getSellerPswd());
        }else if(sellerPassword.checkCondidateIsConfirmed() == SellerPassword.OK){
            pinCodeInst.setState(sellerPassword.authUsingCurrentPassword(pinCode));
            pinCodeInst.setHashedPassword(sellerPassword.getSellerPswd());
        }
        
        return pinCodeInst;
    }
    
    
    
    public static int toInt(String input){ 
        if(input != null && input != "")
            return Integer.valueOf(input);
        return Const.INTEGER_EMPTY_OR_NOT;
    }
    
    public static String toString(int input){ 
        if(input >= 0)
            return String.valueOf(input);
        return "";
    }
    
    public static int getPinCode(){
        Random rand = new Random();
        return rand.nextInt(10000);
    }
    
    public static void raise403(HttpServletResponse response) throws IOException{
        String errorMessage = "Login Error: wrong password!";
        response.setHeader("X-Error-Message", errorMessage);
        response.sendError(HttpServletResponse.SC_FORBIDDEN, errorMessage);
    }
    
    public static void raise404(HttpServletResponse response) throws IOException{
        String errorMessage = "NOT FOUND!";
        response.setHeader("X-Error-Message", errorMessage);
        response.sendError(HttpServletResponse.SC_NOT_FOUND, errorMessage);
    }
    
    
    public static void raise500(HttpServletResponse response) throws IOException{
        final String ERROR_MESSAGE = "INTERNAL SERVER ERROR";
        response.setHeader("X-Error-Message", ERROR_MESSAGE);
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ERROR_MESSAGE);
    }    
    
}
