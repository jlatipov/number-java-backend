package utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jamshed.Latipov
 */


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import oracle.ucp.jdbc.PoolDataSourceFactory;
import oracle.ucp.jdbc.PoolDataSource;

public class DB {

    private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
    private static final String DB_CONNECTION = "jdbc:oracle:thin:@10.84.33.2:1521:ppcdb";
    private static final String DB_CONNECTION_FACTORY = "oracle.jdbc.pool.OracleDataSource";
    private static final String DB_USER = "CCH";
    private static final String DB_PASSWORD = "cch_password";
    private static final int DB_POOL_MIN = 5;
    private static final int DB_POOL_MAX = 10;
    private static final int DB_POOL_INIT = DB_POOL_MIN;
    public static Connection conn;
        
		
    public static Connection getDBConnection() throws SQLException {
        
        if(DB.conn != null){
            System.out.println("has connection");
            return DB.conn;
        }
        
        System.out.println("create connection");
        
         //Creating a pool-enabled data source
        PoolDataSource pds = PoolDataSourceFactory.getPoolDataSource();
        //Setting connection properties of the data source
        pds.setConnectionFactoryClassName(DB_CONNECTION_FACTORY);
        pds.setURL(DB_CONNECTION);
        pds.setUser(DB_USER);
        pds.setPassword(DB_PASSWORD);
        //Setting pool properties

        pds.setInitialPoolSize(DB_POOL_INIT);
        pds.setMinPoolSize(DB_POOL_MIN);
        pds.setMaxPoolSize(DB_POOL_MAX);
        //Borrowing a connection from the pool
        
        DB.conn = pds.getConnection();
        
        return DB.conn;


    }

    private void fillCallableStatement
        ( CallableStatement  stmt, List<?> params) throws SQLException {
        int arraySize = params.size();
        
        for(int i=0; i<arraySize; i++){
            setType(stmt, params.get(i), i);
        }
    }

    private void fillCallableStatement
        ( PreparedStatement  stmt, List<?> params) throws SQLException {
        int arraySize = params.size();
        
        for(int i=0; i<arraySize; i++){
            setType(stmt, params.get(i), i);
        }
        
    }

    public void prepareQuery(
            CallableStatement  stmt, List<?> params) throws SQLException{

        fillCallableStatement(stmt, params);
    }
    
    public void prepareQuery(
            PreparedStatement  stmt, List<?> params) throws SQLException{
        
        fillCallableStatement(stmt, params);
    }
    

    private void setType
        (PreparedStatement  stmt, Object prop, int index) throws SQLException {
        if(prop instanceof Double)
            stmt.setDouble(index, (Double) prop);
        else if(prop instanceof Integer)
            stmt.setInt(index, (Integer) prop);
        else if(prop instanceof Integer)
            stmt.setString(index, (String) prop);
        
    }

    private void setType
        (CallableStatement  stmt, Object prop, int index) throws SQLException {
        if(prop instanceof Double)
            stmt.setDouble(index, (Double) prop);
        else if(prop instanceof Integer)
            stmt.setInt(index, (Integer) prop);
        else if(prop instanceof Integer)
            stmt.setString(index, (String) prop);
        
    }


}
