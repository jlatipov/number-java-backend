/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.api.TariffPlan;
import models.api.TariffPlans;
import utils.Const;
import utils.Helper;

/**
 *
 * @author jamshed.latipov
 */
public class getTariffPlans extends HttpServlet {
    private static final String SELLER_ID = "seller_id";
    private static final String MSISDN = "msisdn";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType(Const.HEADER_JSON);
        PrintWriter out = response.getWriter();
        
        // raising 403 when user try to get access to page using GET method
        if(!request.getMethod().equals(Const.HTTP_METHOD_POST)){
            Helper.raise403(response);
        }
        
        try {
            TariffPlans tariffPlans = new TariffPlans();
            TariffPlan tariffPlan = new TariffPlan();
            int seller_id = Integer.valueOf(request.getParameter(SELLER_ID));
            String msisdn = request.getParameter(MSISDN);

            tariffPlans.setTariffPlans(tariffPlan.list(seller_id, msisdn));
            Gson gson = new Gson();
            String json = gson.toJson(tariffPlans);
            out.println(json);

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(getTariffPlans.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(getTariffPlans.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
