/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.api.FillFormModel;

/**
 *
 * @author jamshed.latipov
 */
public class FillForm extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ParseException {
        
        request.setCharacterEncoding("UTF-8");
        
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        final int CHI_GAP_TARIFF_PLAN = 491;

        
        try {
            if(request.getMethod().equals("POST")){
                
                
                
                String phoneNumber = request.getParameter("strPhoneNumber");
                
                String strName = request.getParameter("strName");
                String strSecondName = request.getParameter("strSecondName");
                
                String strLastName = request.getParameter("strLastName");
                
                String strIcc = request.getParameter("strIcc");
                
                String txtDateBirth = request.getParameter("txtDateBirth");
                
                String txtPassportWhen = request.getParameter("txtPassportWhen");
                String txtCountry = request.getParameter("txtCountry");
                String txtRegion = request.getParameter("txtRegion");
                String txtDistrict = request.getParameter("txtDistrict");
                String txtCity = request.getParameter("txtCity");
                String txtJamoat = request.getParameter("txtJamoat");
                String txtVillage = request.getParameter("txtVillage");
                String txtStreet = request.getParameter("txtStreet");
                String txtHouse = request.getParameter("txtHouse");
                String txtNumber = request.getParameter("txtPassportNumber");
                String txtPassportPlace = request.getParameter("txtPassportPlace");
                
                String txtPassportSerias = request.getParameter("txtPassportSerias");   
                String contactNumber = request.getParameter("contactNumber");
                
                            
                int regType = Integer.valueOf(request.getParameter("regType"));
                String clientIp = request.getParameter("clientIp");
                int passType = Integer.valueOf(request.getParameter("passType"));
                String email = request.getParameter("email");

                
                int tariffPlanId = 0;
                
                
                if(regType == FillFormModel.NORMAL_REG)
                    tariffPlanId = Integer.valueOf(request.getParameter("tariffPlanId"));
                else if(regType == FillFormModel.CHI_GAP_REG){
                    tariffPlanId = CHI_GAP_TARIFF_PLAN;
                    

                }
                
                
                int gender = Integer.valueOf(request.getParameter("gender"));
                int sellerId = Integer.valueOf(request.getParameter("sellerId"));
                
                
                FillFormModel fillFormModel = new FillFormModel();
                fillFormModel.setPhoneNumber(phoneNumber);
                fillFormModel.setStrName(strName);
                fillFormModel.setGender(gender);
                fillFormModel.setStrSecondName(strSecondName);
                fillFormModel.setStrLastName(strLastName);
                fillFormModel.setStrIcc(strIcc);
                fillFormModel.setTxtDateBirth(txtDateBirth);
                fillFormModel.setTxtPassportGet(txtPassportWhen);
                fillFormModel.setTxtCountry(txtCountry);
                fillFormModel.setTxtRegion(txtRegion);
                fillFormModel.setTxtDistrict(txtDistrict);
                fillFormModel.setTxtCity(txtCity);
                fillFormModel.setTxtJamoat(txtJamoat);
                fillFormModel.setTxtVillage(txtVillage);
                fillFormModel.setTxtStreet(txtStreet);
                fillFormModel.setTxtHouse(txtHouse);
                fillFormModel.setTxtPassportNumber(txtNumber);
                fillFormModel.setTxtPassportPlace(txtPassportPlace);
                fillFormModel.setTariffPlanId(tariffPlanId);
                fillFormModel.setTxtPassportSerias(txtPassportSerias);
                fillFormModel.setSellerId(sellerId);
                fillFormModel.setRegType(regType);
                fillFormModel.setContactNumber(contactNumber);
                fillFormModel.setPassType(passType);
                fillFormModel.setClientIp(clientIp);
                fillFormModel.setEmail(email);
                
                
                
                int result = fillFormModel.insert();
                
                if(result == 0)
                    out.println("{\"state\":\"ICC не прописан!\", \"id\": \"" +result + "\"}");
                else
                    out.println("{\"state\":\"OK\", \"id\": \"" +result + "\"}");
            }
        }catch(Exception ex){
            System.out.println(ex);
            out.println("{\"state\":\"" + ex.toString() + "\"}");
        } finally {
            out.close();
        }
    }
    
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(FillForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(FillForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(FillForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(FillForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
