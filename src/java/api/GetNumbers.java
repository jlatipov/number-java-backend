/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.api.MSISDN;
import models.api.MSISDNS;
import utils.DB;

/**
 *
 * @author jamshed.latipov
 */
public class GetNumbers extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
        if(request.getMethod().equals("POST")){
            
            String pattern = request.getParameter("pattern");
            String prefix = request.getParameter("prefix");
            int phoneType = utils.Helper.toInt(request.getParameter("phoneType"));
            int branchId = utils.Helper.toInt(request.getParameter("branchId"));
            int pageTo = utils.Helper.toInt(request.getParameter("pageTo"));
            
            
            String reversedPattern = "";
            String  reversedPrefix = "";
            
            reversedPattern = new StringBuilder(pattern).reverse().toString();
            reversedPattern = reversedPattern + "%";
            
            
            if(prefix != "" && prefix != null){
                reversedPrefix = new StringBuilder(prefix).reverse().toString();
                reversedPattern = reversedPattern + reversedPrefix;
//                reversedPrefix = "%" + reversedPrefix;
            }
            
            
            
            
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter out = response.getWriter();
            try {
                
                MSISDNS msisdns = new MSISDNS();
                ArrayList<MSISDN> msisdnList = new MSISDN().list(phoneType, pageTo, reversedPattern, branchId);
                msisdns.setMsisdn(msisdnList);
                String json = new Gson().toJson(msisdns);
                out.println(json);

            } finally {
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GetNumbers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GetNumbers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
