package api;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.api.PinCode;
import models.api.SellerPassword;
import utils.Const;
import utils.Hash;
import utils.Helper;

/**
 *
 * @author jamshed.latipov
 */
public class SellerAuth extends HttpServlet {
    
    public static final int STATE_PASSWORD_EXPIRED = -2;
    public static final int STATE_LOCK = -1;
    public static final int STATE_OK = 1;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try {
            if(request.getMethod().equals("POST")){
                // getting params
                String pinCode = "";
                
//                if(request.getParameter("pincode").length() > 8)
//                    pinCode = request.getParameter("pincode");
//                else
                    pinCode = Hash.md5Custom(request.getParameter("pincode"));
                    System.out.println(pinCode + "--------------pin code");
                
                int sellerId = Integer.valueOf(request.getParameter("sellerId"));
                
                
                PinCode pinCodeInst = Helper.auth(pinCode, sellerId);
                
                Gson gson = new Gson();
                String json = gson.toJson(pinCodeInst);
                out.println(json);
                
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(SellerAuth.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(SellerAuth.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
