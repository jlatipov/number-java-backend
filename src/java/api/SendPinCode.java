/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.api.Seller;
import models.api.SellerPassword;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OraclePreparedStatement;
import utils.DB;


import oracle.jdbc.pool.*;
import utils.Const;
import utils.Helper;
import utils.SMS;

/**
 *
 * @author Jamshed.Latipov
 */
public class SendPinCode extends HttpServlet {
    private final String IMSI_REQUEST = "imsi";
    private final String MSISDN_REQUEST = "msisdn";
    private final String SOURCE_REQUEST = "source";
    private final String SOURCE_WEB = "WEB";
    private final String SOURCE_ANDROID = "ANDROID";
    private final String SOURCE_IOS = "IOS";
    
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        String imsi = "";
        String source = "";
        String msisdn = "";
        
        if(request.getMethod().equals("POST")){
            try{
                imsi = request.getParameter(IMSI_REQUEST);
                source = request.getParameter(SOURCE_REQUEST);
                msisdn = request.getParameter(MSISDN_REQUEST);
                
                Seller seller = new Seller();
                if(source.equals(SOURCE_WEB))
                    seller.fillByMsisdn(msisdn);
                else if(source.equals(SOURCE_ANDROID)) 
                    seller.fill(imsi);
                
                
                if(seller.getState() != Const.SELLER_DOES_NOT_EXIST){
                    
                    System.out.println(seller.getMsisdn() + "---- msisdn");
                    
                    if(Const.SELLER_WAS_LOCKED != seller.getUserState()){
                        final String pinCode = String.valueOf(Helper.getPinCode());
                        boolean smsResult = new SMS(seller.getMsisdn(), pinCode).sendSMS();

                        boolean passwordAlradyExsist = false;
                        if(smsResult){
                           SellerPassword sellerPassword = new SellerPassword();
                            
                           int sellerId = seller.getId();
                           try{
                               if(sellerPassword.find(sellerId))
                                   passwordAlradyExsist = true;
                           }catch(Exception ex){
                               System.out.println(ex);
                           }

                           sellerPassword.setSellerId(sellerId);
                           sellerPassword.setSellerPswdCondidate(pinCode);
                           sellerPassword.setSellerPswdCondidateStatus(SellerPassword.PASSWORD_CONDIDATE_NOT_CONFIRMED);
                           
                           if(passwordAlradyExsist)
                               sellerPassword.update(sellerPassword.getPswdId());
                           else
                               sellerPassword.insert();
                        }else{
                            seller.setState(Const.SMS_NOT_SEND);
                        }
                    }
                }
                 
                String json = new Gson().toJson(seller);
                out.println(json);
                 
            } catch (Exception ex) {
                out.println(ex.toString());
                System.out.println(ex);
            } finally {
                out.close();
            }
        }else{
            utils.Helper.raise403(response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
