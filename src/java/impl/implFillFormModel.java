/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 *
 * @author jamshedlatipov
 */
public interface implFillFormModel {
    public int insert() throws SQLException, IOException, ParseException;
    public String bpRegister(int order_id) throws SQLException;
    public boolean changeState(int id);
    public boolean uploadImage(String type, int orderId);
}
