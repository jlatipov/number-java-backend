/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import java.sql.SQLException;
import models.api.Locations;

/**
 *
 * @author jamshedlatipov
 */

public interface implLocation {
    Locations getLocations(String object, String pattern) throws SQLException;
}
