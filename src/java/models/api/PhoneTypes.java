/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.util.ArrayList;

/**
 *
 * @author jamshed.latipov
 */
public class PhoneTypes {
    private ArrayList<PhoneType> phoneTypes;

    /**
     * @return the phoneTypes
     */
    public ArrayList<PhoneType> getPhoneTypes() {
        return phoneTypes;
    }

    /**
     * @param phoneTypes the phoneTypes to set
     */
    public void setPhoneTypes(ArrayList<PhoneType> phoneTypes) {
        this.phoneTypes = phoneTypes;
    }
}
