/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.util.ArrayList;

/**
 *
 * @author jamshed.latipov
 */
public class TariffPlans {
    private ArrayList<TariffPlan> tariffPlans;

    /**
     * @return the tariffPlans
     */
    public ArrayList<TariffPlan> getTariffPlans() {
        return tariffPlans;
    }

    /**
     * @param tariffPlans the tariffPlans to set
     */
    public void setTariffPlans(ArrayList<TariffPlan> tariffPlans) {
        this.tariffPlans = tariffPlans;
    }
    
    
}
