/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import oracle.jdbc.OracleTypes;
import utils.Const;
import utils.DB;
import utils.Helper;
import utils.HelperModels;
import utils.SQL;

/**
 *
 * @author jamshed.latipov
 */
public class Report {
    private int id;
    private int Status;
    private String fullName;
    private String regDate;
    private String icc;
    private String number;
    private String msg;

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the regDate
     */
    public String getRegDate() {
        return regDate;
    }

    /**
     * @param regDate the regDate to set
     */
    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    /**
     * @return the icc
     */
    public String getIcc() {
        return icc;
    }

    /**
     * @param icc the icc to set
     */
    public void setIcc(String icc) {
        this.icc = icc;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    
    public ArrayList<Report> getReport(int sellerId) throws ParseException, SQLException {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        Calendar cal = Calendar.getInstance();
        Connection conn =  DB.getDBConnection();
        conn.setAutoCommit(false);
        
        
//        java.sql.Date bornDate = new java.sql.Date(df.parse(cal.getTime().toString()).getDate());
        String query = SQL.GET_REPORT_BY_SELLER_ID;
        ArrayList<Report> reports = new ArrayList<Report>();
        CallableStatement  stmt = conn.prepareCall(query);
        try{
            stmt.setInt(1, sellerId);
            ResultSet rs = stmt.executeQuery();
            try{
                while (rs.next()){
                    Report report = new Report();
                    report.regDate = rs.getString("cre_date");
                    report.icc = rs.getString("icc");
                    report.number = rs.getString("msisdn");
                    report.Status = rs.getInt("status");
                    report.id = rs.getInt("id");
                    report.fullName = 
                            rs.getString("last_name") 
                            + " " + rs.getString("name")
                            + " " + rs.getString("middle_name");
                    reports.add(report);
                }
            }catch(Exception ex){
                System.out.println("asldkhasgdgasdg"+ex);
            }finally{
                rs.close();
            }
        }catch(Exception ex){
                System.out.println(ex);
        }finally{
            stmt.close();
        }

        return reports;
    }
    
    
    public ArrayList<Report> getReport(int sellerId, String msisdn) throws ParseException, SQLException {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        Calendar cal = Calendar.getInstance();
        Connection conn =  DB.getDBConnection();
        conn.setAutoCommit(false);
        
        
//        java.sql.Date bornDate = new java.sql.Date(df.parse(cal.getTime().toString()).getDate());
        String query = SQL.GET_REPORT_BY_SELLER_ID_AND_MSISDN;
        
        ArrayList<Report> reports = new ArrayList<Report>();
        CallableStatement  stmt = conn.prepareCall(query);
        try{
            stmt.setInt(1, sellerId);
            stmt.setString(2, msisdn);
            ResultSet rs = stmt.executeQuery();
            try{
                while (rs.next()){
                    Report report = new Report();
                    report.fullName = rs.getString("subs_name");
                    report.regDate = rs.getString("cre_date");
                    report.icc = rs.getString("icc");
                    report.number = rs.getString("msisdn");
                    report.Status = rs.getInt("status");
                    report.msg = rs.getString("last_msg");
                    report.id = rs.getInt("id");
                    reports.add(report);
                }
            }finally{
                rs.close();
            }
        }finally{
            stmt.close();
        }
        
        
        return reports;
    }
    
    public ArrayList<Report> getReport(int sellerId, String msisdn, String dateFrom, String dateTo) throws ParseException, SQLException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Connection conn =  DB.getDBConnection();
        conn.setAutoCommit(false);
        
        
        java.sql.Date dateFromSql = new java.sql.Date(df.parse(dateFrom).getDate());
        java.sql.Date dateToSql = new java.sql.Date(df.parse(dateTo).getDate());
        String query = SQL.GET_REPORT_BY_SELLER_ID_AND_MSISDN_AND_DATE;
        
        ArrayList<Report> reports = new ArrayList<Report>();
        CallableStatement  stmt = conn.prepareCall(query);
        try{
            stmt.setInt(1, sellerId);
            stmt.setString(2, msisdn);
            stmt.setDate(3, dateFromSql);
            stmt.setDate(4, dateToSql);
            ResultSet rs = stmt.executeQuery();
            try{
                while (rs.next()){
                    Report report = new Report();
                    report.fullName = rs.getString("subs_name");
                    report.regDate = rs.getString("cre_date");
                    report.icc = rs.getString("icc");
                    report.number = rs.getString("msisdn");
                    report.Status = rs.getInt("status");
                    report.msg = rs.getString("last_msg");
                    report.id = rs.getInt("id");
                    reports.add(report);
                }
            }finally{
                rs.close();
            }
        }finally{
            stmt.close();
        }
        
        
        return reports;
    }
    
    
    public void getReportByOrderId(int orderId) throws ParseException, SQLException {
        Connection conn =  DB.getDBConnection();
        conn.setAutoCommit(false);
        
        
        String query = SQL.GET_REPORT_BY_ORDER_ID;
        
        
        CallableStatement  stmt = conn.prepareCall(query);
        try{
            stmt.setInt(1, orderId);
            ResultSet rs = stmt.executeQuery();
            try{
                while (rs.next()){
                    regDate = rs.getString("cre_date");
                    icc = rs.getString("icc");
                    number = rs.getString("msisdn");
                    Status = rs.getInt("status");
                    id = rs.getInt("id");
                }
            }finally{
                rs.close();
            }
        }finally{
            stmt.close();
        }
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the Status
     */
    public int getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(int Status) {
        this.Status = Status;
    }
    
}
