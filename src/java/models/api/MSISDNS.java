/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.util.ArrayList;

/**
 *
 * @author jamshed.latipov
 */
public class MSISDNS {
    private ArrayList<MSISDN> msisdn; 

    /**
     * @return the msisdn
     */
    public ArrayList<MSISDN> getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(ArrayList<MSISDN> msisdn) {
        this.msisdn = msisdn;
    }
}
