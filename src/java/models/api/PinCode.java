/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

/**
 *
 * @author jamshed.latipov
 */
public class PinCode {
    public static final int STATE_PASSWORD_NOT_EQUAL = -3;
    public static final int STATE_PASSWORD_EXPIRED = -2;
    public static final int STATE_LOCK = -1;
    public static final int STATE_OK = 1;



    private String msisdn;
    private int state;
    private String hashedPassword;


    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    /**
     * @return the hashedPassword
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     * @param hashedPassword the hashedPassword to set
     */
    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }
}
