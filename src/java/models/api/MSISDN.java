/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utils.Const;
import utils.DB;
import utils.Helper;
import utils.HelperModels;
import utils.SQL;

/**
 *
 * @author jamshed.latipov
 */


public class MSISDN {
    private int id;
    private int branchId;
    private String mask;
    private String msisdn;
    private String phoneClass;
    private float price;
    
    public int getId() {
        return id;
    }
    
    
    public int getById(int id) throws SQLException{
        String sql = SQL.GET_MSISDN_BY_ID;
        Connection dbConnection = DB.getDBConnection();
        PreparedStatement pstmt  = dbConnection.prepareStatement(sql);
        try{
            pstmt.setInt(1, id);

            ResultSet rs = pstmt.executeQuery();
            try{
                if(rs.next()){
                    this.setId(rs.getInt("phone_id"));
                    this.setBranchId(rs.getInt("branch_id"));
                    this.setMask(rs.getString("mask"));
                    this.setMsisdn(rs.getString("msisdn"));
                    this.setPhoneClass(rs.getString("phone_type"));
                    this.setPrice(Helper.getNumberPrice(this.getMsisdn()));
                    return Const.OK;
                }
            }finally{
                rs.close();
            }
        }finally{
            pstmt.close();
        }
        return Const.DB_CANNOT_FIND_RECORD;
    }
    
    
    
    public ArrayList<MSISDN> list(int phoneType, int pageTo, String pattern, int branchId) throws SQLException{
        String sql = "";
        if(phoneType == 0){
            sql = SQL.GET_MSISDN_LIST_WITHOUT_PHONE_TYPE;
        }else{
            sql = SQL.GET_MSISDN_LIST_WITH_PHONE_TYPE;
        }
        

        Connection dbConnection = DB.getDBConnection();
        PreparedStatement pstmt  = dbConnection.prepareStatement(sql);
        ArrayList<MSISDN> msisdns = new ArrayList<MSISDN>();
        
        try{

            if(phoneType != 0){
                pstmt.setString(1, pattern);
                pstmt.setInt(2, branchId);
                pstmt.setInt(3, phoneType);
                pstmt.setInt(4, pageTo);
                pstmt.setInt(5, pageTo);

            }else{            
                pstmt.setString(1, pattern);
                pstmt.setInt(2, branchId);
                pstmt.setInt(3, pageTo);
                pstmt.setInt(4, pageTo);

            }


            

            ResultSet rs = pstmt.executeQuery();
            try{
                while(rs.next()){
                    MSISDN msisdn = new MSISDN();
                    msisdn.setId(rs.getInt("phone_id"));
                    msisdn.setBranchId(rs.getInt("branch_id"));
                    msisdn.setMask(rs.getString("mask"));
                    msisdn.setMsisdn(rs.getString("msisdn"));
                    msisdn.setPhoneClass(rs.getString("phone_type"));
                    msisdns.add(msisdn);
                }
            }finally{
                rs.close();
            }
        }finally{
            pstmt.close();
        }

        return msisdns;
    }
    
    
    
     public ArrayList<MSISDN> listChiGap(int pageTo, String pattern) throws SQLException{
        String sql = SQL.GET_CHI_GAP_MSISDN;

        Connection dbConnection = DB.getDBConnection();
        PreparedStatement pstmt  = dbConnection.prepareStatement(sql);
        ArrayList<MSISDN> msisdns = new ArrayList<MSISDN>();
        
        try{
            
            pstmt.setString(1, pattern);
            pstmt.setInt(2, pageTo);
            pstmt.setInt(3, pageTo);

            
            ResultSet rs = pstmt.executeQuery();
            try{
                while(rs.next()){
                    MSISDN msisdn = new MSISDN();
                    msisdn.setId(rs.getInt("phone_id"));
                    msisdn.setBranchId(rs.getInt("branch_id"));
                    msisdn.setMsisdn(rs.getString("msisdn"));
                    msisdn.setPhoneClass(rs.getString("phone_type"));
                    msisdns.add(msisdn);
                }
            }catch(Exception e){
                System.out.println(e);
            }finally{
                rs.close();
            }
        }catch(Exception e){
                System.out.println(e);
        }finally{
            pstmt.close();
        }

        return msisdns;
    }
    

    
    public static ArrayList<Integer> getBranch(String msisdn) throws SQLException{
        
        Connection conn =  DB.getDBConnection();
        ArrayList<Integer> params = new ArrayList<Integer>();
        
        
        String sql = SQL.GET_MSISDN_BRANCH;
        
        PreparedStatement stmt = conn.prepareStatement(sql);
        try{
            stmt.setString(1, msisdn);

            ResultSet rs = stmt.executeQuery();
            try{
                if(rs.next()){
                    params.add(rs.getInt("branch")); // 0 branch
                    params.add(rs.getInt("switch")); // 1 switch
                }else{
                    return null;
                }
            }finally{
                rs.close();
            }
            
        }finally{
            stmt.close();
        }
        
        
        return params;
        
    }
    
    public static String getIcc(String icc, String msisdn) throws SQLException{
        String outIcc = null;
        
        Connection conn =  DB.getDBConnection();
        String sql = SQL.GET_MSISDN_ICC;
        
        PreparedStatement stmt = conn.prepareStatement(sql);
        try{
            String reversedMsisdn = new StringBuilder(msisdn).reverse().toString();
            reversedMsisdn += "%";
            ArrayList<Integer> iccParams = getBranch(msisdn);
            int branch = iccParams.get(0);
            stmt.setInt(1, branch);
            stmt.setString(2, reversedMsisdn);


            ResultSet rs = stmt.executeQuery();
            try{
                if(rs.next()){
                    outIcc = rs.getString("icc");
                }else{
                    return null;
                }
            }finally{
                rs.close();
            }
        }finally{
            stmt.close();
        }
        
        return outIcc;
    }
    
    
    
    public static HelperModels.DealerAndIcc getDealer(String icc, String msisdn) throws SQLException{
        HelperModels.DealerAndIcc dealerAndIcc = new HelperModels.DealerAndIcc();
        
        Connection conn =  DB.getDBConnection();
        conn.setAutoCommit(false);
        String sql = SQL.GET_MSISDN_DEALER;
        
        PreparedStatement stmt = conn.prepareStatement(sql);
        try{
            String reversedIcc = new StringBuilder(icc).reverse().toString();
            reversedIcc += "%";
            ArrayList<Integer> branchParams = getBranch(msisdn);

            stmt.setInt(1, branchParams.get(0)); // 0 branchId     
            stmt.setInt(2, branchParams.get(1)); // 1 switchId
            stmt.setString(3, reversedIcc);
            System.out.println(branchParams.get(1) + "sWITCH ID");
            ResultSet rs = stmt.executeQuery();
            try{
                if(rs.next()){
                    dealerAndIcc.setDealer(rs.getInt("dealer"));
                    dealerAndIcc.setIcc(rs.getString("icc"));
                    dealerAndIcc.setState(Const.OK);
                }else{
                    dealerAndIcc.setState(Const.DB_CANNOT_FIND_RECORD);
                    return dealerAndIcc;
                }
            }finally{
                rs.close();
            }
        }finally{
            stmt.close();
        }
        
        return dealerAndIcc;
    }
    
    
    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPhoneClass() {
        return phoneClass;
    }
    
    public void  setPhoneClass(String phoneClass) {
        this.phoneClass = phoneClass;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the mask
     */
    public String getMask() {
        return mask;
    }

    /**
     * @param mask the mask to set
     */
    public void setMask(String mask) {
        this.mask = mask;
    }
}
