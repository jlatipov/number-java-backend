/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utils.Const;
import utils.DB;
import utils.SQL;

/**
 *
 * @author jamshed.latipov
 */
public class Balance  implements impl.ImplBalance{

    
    private int state;
    private String msg;
    private float amount;


    public Balance(int state, String msg, float amount) {
        this.state = state;
        this.amount = amount;

        this.msg = msg;
    }
    
    public Balance() {}
    
    
    
    public float getBalance(String msisdn) throws SQLException{
        String sql = SQL.GET_BALANCE;
        Connection dbConnection = DB.getDBConnection();
        PreparedStatement pstmt  = dbConnection.prepareStatement(sql);
        try{
            pstmt.setString(1, msisdn);
            ResultSet rs = pstmt.executeQuery();
            try{
                if(rs.next()){
                    this.state = 0;
                    this.msg = Const.OK_MESSAGE;
                    this.amount = rs.getFloat("bal");
                }else{
                    System.out.println("");
                    this.state = 1;
                    this.msg = Const.DB_CANNOT_FIND_RECORD_MESSAGE;
                    this.amount = 0;
                }
            }finally{
                rs.close();
            }
        }finally{
            pstmt.close();
        }

        return 0;
    }
    

    /**
     * @return the state
     */
    public int getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    
    
}
