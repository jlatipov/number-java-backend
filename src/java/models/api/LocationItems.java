/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utils.DB;

/**
 *
 * @author jamshed.latipov
 */

public class LocationItems {
    private int id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Locations getLocations(String object, String pattern) throws SQLException{
        pattern = "%" + pattern + "%";
        String sql = "select name from cch."+getObject(object)+" where name like ?";
        System.out.println(sql);
        Connection dbConnection = DB.getDBConnection();
        PreparedStatement pstmt  = dbConnection.prepareStatement(sql);
        Locations locations = new Locations();
        try{
            pstmt.setString(1, pattern);
            ResultSet rs = pstmt.executeQuery();
            try{
                while(rs.next()){
                    LocationItems location = new LocationItems();
                    location.id = 0;
                    location.name = rs.getString("name");
                    locations.locationItems.add(location);
                }
            }finally{
                rs.close();
            }
        }finally{
            pstmt.close();
        }
        
        return locations;
    }

    
    private String getObject(String obj){
        
        if(obj.equals("country"))
            return "V_WD_COUNTRIES";
        if(obj.equals("region"))
            return "V_WD_COUNTRY_REGIONS";
        if(obj.equals("district"))
            return "V_WD_COUNTRY_DISTRICTS";
        if(obj.equals("city"))
            return "V_WD_CITIES";
        else
            return null;
    }
    
    public int getId() {
        return id;
        
    }

    public void setId(int id) {
        this.id = id;
    }
}
