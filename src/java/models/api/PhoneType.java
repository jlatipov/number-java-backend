/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utils.Const;
import utils.DB;
import utils.SQL;

/**
 *
 * @author jamshed.latipov
 */
public class PhoneType {
    private int id;
    private String name;
    
    
    public ArrayList<PhoneType> list() throws SQLException{
        
        String sql = SQL.GET_PHONE_TYPE;
        Connection conn = DB.getDBConnection();
        PreparedStatement  prepre = conn.prepareStatement(sql);
        ArrayList<PhoneType> phoneTypes = new ArrayList<PhoneType>();
        try{
            ResultSet rs = prepre.executeQuery();
            
            try{
                while(rs.next()){
                    PhoneType phoneType =  new PhoneType();
                    phoneType.setId(rs.getInt("pht_id"));
                    phoneType.setName(rs.getString("phone_type"));
                    phoneTypes.add(phoneType);
                }
            }finally{
                rs.close();
            }
        }finally{
            prepre.close();
        }
        
        return phoneTypes;
    }
    
    
    
    public int getById(int id) throws SQLException{
        String sql = SQL.GET_PHONE_TYPE_BY_ID;
        
        Connection conn = DB.getDBConnection();
        PreparedStatement  prepre = conn.prepareStatement(sql);
        try{
            prepre.setInt(1, id);
            ResultSet rs = prepre.executeQuery();
            try{
                if(rs.next()){
                    this.setId(rs.getInt("pht_id"));
                    this.setName(rs.getString("phone_type"));
                    return Const.OK;
                }
            }finally{
                rs.close();
            }
        }finally{
            prepre.close();
        }
        return Const.DB_CANNOT_FIND_RECORD; 
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
