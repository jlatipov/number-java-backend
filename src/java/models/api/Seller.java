/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utils.Const;
import utils.DB;
import utils.SQL;

/**
 *
 * @author Jamshed.Latipov
 */
public class Seller {
    private int id;
    private String sellerPointId;
    private int regionId;
    private String address;
    private int paymentSystem;
    private String sellerName;
    private String sellerContactPhone;
    private String spCategory;
    private String spOwnerPhone;
    private float spSquere;
    private String createdAt;
    private String updatedAt;
    private String registrationDate;
    private int branchId;
    
    private int dealerId;
    
    private String msisdn;
    private int userState;
    private int state = Const.DB_CANNOT_FIND_RECORD;
    
    
    
    
    public void findById(int sellerId) throws SQLException{
        Connection conn = DB.getDBConnection();
        try {
            String query = SQL.GET_SELLER_BY_ID;
            
            PreparedStatement pstmt = conn.prepareStatement(query);
            try{
                pstmt.setInt(1, sellerId);

                ResultSet rs =  pstmt.executeQuery();
                try{
                    if(rs.next()){
                            this.setMsisdn(rs.getString("msisdn"));
                            this.setUserState(rs.getInt("status"));
                            this.setId(rs.getInt("SELLER_ID"));
                            this.setSellerPointId(rs.getString("SALE_POINT_STR_ID"));
                            this.setAddress(rs.getString("ADDRESS"));
                            this.setSellerName(rs.getString("SELLER_NAME"));
                            this.setSellerContactPhone(rs.getString("SELLER_CONTACT_PHN"));
                            this.setSpCategory(rs.getString("SP_CATEGORY"));
                            this.setSpOwnerPhone(rs.getString("SP_OWNER_PHONE"));
                            this.setSpSquere(rs.getFloat("SP_SQUERE"));
                            this.setRegistrationDate(rs.getString("REGISTRATION_DATE"));
                            this.setDealerId(rs.getInt("DEALER_ID"));
                            this.setBranchId(rs.getInt("branch_id"));

                            this.setRegionId(rs.getInt("REGION_ID"));
                            this.setState(Const.OK);
                    }else{
                        this.setState(Const.SELLER_DOES_NOT_EXIST);
                    }
                }finally{
                    rs.close();
                }
            }finally{
                pstmt.close();
            }

        } catch(SQLException ex){

        }
    }
    
    
    
    
    public void fill(String imsi) throws SQLException{
        Connection conn = DB.getDBConnection();
        try {
            String query = SQL.GET_SELLER_BY_IMSI;
            
            PreparedStatement pstmt = conn.prepareStatement(query);
            try{
                pstmt.setString(1, imsi);
                ResultSet rs =  pstmt.executeQuery();
                try{
                    if(rs.next()){
                            this.setMsisdn(rs.getString("msisdn"));
                            this.setUserState(rs.getInt("status"));
                            this.setId(rs.getInt("SELLER_ID"));
                            this.setSellerPointId(rs.getString("SALE_POINT_STR_ID"));
                            this.setAddress(rs.getString("ADDRESS"));
                            this.setSellerName(rs.getString("SELLER_NAME"));
                            this.setSellerContactPhone(rs.getString("SELLER_CONTACT_PHN"));
                            this.setSpCategory(rs.getString("SP_CATEGORY"));
                            this.setSpOwnerPhone(rs.getString("SP_OWNER_PHONE"));
                            this.setSpSquere(rs.getFloat("SP_SQUERE"));
                            this.setRegistrationDate(rs.getString("REGISTRATION_DATE"));
                            this.setDealerId(rs.getInt("DEALER_ID"));
                            this.setBranchId(rs.getInt("branch_id"));

                            this.setRegionId(rs.getInt("REGION_ID"));
                            this.setState(Const.OK);
                    }else{
                        this.setState(Const.SELLER_DOES_NOT_EXIST);
                    }
                }finally{
                    rs.close();
                }
            }finally{
                pstmt.close();
            }
            
        } catch(SQLException ex){

        }
    }
    
    public void fillByMsisdn(String msisdn) throws SQLException{
        Connection conn = DB.getDBConnection();
        try {
            String query = SQL.GET_SELLER_BY_MSISDN;
            
            PreparedStatement pstmt = conn.prepareStatement(query);
            try{
                pstmt.setString(1, msisdn);
                ResultSet rs =  pstmt.executeQuery();

                try{
                    if(rs.next()){
                            this.setMsisdn(rs.getString("msisdn"));
                            this.setUserState(rs.getInt("status"));
                            this.setId(rs.getInt("SELLER_ID"));
                            this.setSellerPointId(rs.getString("SALE_POINT_STR_ID"));
                            this.setAddress(rs.getString("ADDRESS"));
                            this.setSellerName(rs.getString("SELLER_NAME"));
                            this.setSellerContactPhone(rs.getString("SELLER_CONTACT_PHN"));
                            this.setSpCategory(rs.getString("SP_CATEGORY"));
                            this.setSpOwnerPhone(rs.getString("SP_OWNER_PHONE"));
                            this.setSpSquere(rs.getFloat("SP_SQUERE"));
                            this.setRegistrationDate(rs.getString("REGISTRATION_DATE"));
                            this.setDealerId(rs.getInt("DEALER_ID"));
                            this.setBranchId(rs.getInt("branch_id"));

                            this.setRegionId(rs.getInt("REGION_ID"));
                            this.setState(Const.OK);
                    }else{
                        this.setState(Const.SELLER_DOES_NOT_EXIST);
                    }
                }finally{
                    rs.close();
                }
            }finally{
                pstmt.close();
            }

        } catch(SQLException ex){
            System.out.println(ex.toString());
        }
    }
    
   
    
    public String getMsisdn() {
        final String  PREFIX = "992";
        if(this.msisdn.length() == 9)
            return PREFIX + msisdn;
        else 
            return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public int getUserState() {
        return userState;
    }

    public void setUserState(int userState) {
        this.userState = userState;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
    
    

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the sellerPointId
     */
    public String getSellerPointId() {
        return sellerPointId;
    }

    /**
     * @return the regionId
     */
    public int getRegionId() {
        return regionId;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @return the paymentSystem
     */
    public int getPaymentSystem() {
        return paymentSystem;
    }

    /**
     * @return the sellerName
     */
    public String getSellerName() {
        return sellerName;
    }

    /**
     * @return the sellerContactPhone
     */
    public String getSellerContactPhone() {
        return sellerContactPhone;
    }

    /**
     * @return the spCategory
     */
    public String getSpCategory() {
        return spCategory;
    }

    /**
     * @return the spOwnerPhone
     */
    public String getSpOwnerPhone() {
        return spOwnerPhone;
    }

    /**
     * @return the spSquere
     */
    public float getSpSquere() {
        return spSquere;
    }

    /**
     * @return the createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @return the updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param sellerPointId the sellerPointId to set
     */
    public void setSellerPointId(String sellerPointId) {
        this.sellerPointId = sellerPointId;
    }

    /**
     * @param regionId the regionId to set
     */
    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @param paymentSystem the paymentSystem to set
     */
    public void setPaymentSystem(int paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    /**
     * @param sellerName the sellerName to set
     */
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    /**
     * @param sellerContactPhone the sellerContactPhone to set
     */
    public void setSellerContactPhone(String sellerContactPhone) {
        this.sellerContactPhone = sellerContactPhone;
    }

    /**
     * @param spCategory the spCategory to set
     */
    public void setSpCategory(String spCategory) {
        this.spCategory = spCategory;
    }

    /**
     * @param spOwnerPhone the spOwnerPhone to set
     */
    public void setSpOwnerPhone(String spOwnerPhone) {
        this.spOwnerPhone = spOwnerPhone;
    }

    /**
     * @param spSquere the spSquere to set
     */
    public void setSpSquere(float spSquere) {
        this.spSquere = spSquere;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the registrationDate
     */
    public String getRegistrationDate() {
        return registrationDate;
    }

    /**
     * @param registrationDate the registrationDate to set
     */
    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * @return the dealerId
     */
    public int getDealerId() {
        return dealerId;
    }

    /**
     * @param dealerId the dealerId to set
     */
    public void setDealerId(int dealerId) {
        this.dealerId = dealerId;
    }

    /**
     * @return the branchId
     */
    public int getBranchId() {
        return branchId;
    }

    /**
     * @param branchId the branchId to set
     */
    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }
}
