/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import com.google.gson.annotations.Expose;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.StructDescriptor;
import utils.DB;
import utils.SQL;

/**
 *
 * @author jamshed.latipov
 */

public class TariffPlan {
    
    private String name;
    private int id;
    private static final String NEXT_PREFIX = "50";
    private static final String NEXT_ID = "481";
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    private boolean isNext(String msisdn){
        String prefix = msisdn.substring(3, 5);
        if(NEXT_PREFIX.equals(prefix))
            return true;
        else
            return false;
    }
    
    private String getTariffPlans(ArrayList<String> enableTariffPlanes, String msisdn){
        String forBind = "";
        boolean isFirst = true;
        for(String s : enableTariffPlanes){
            if(isNext(msisdn)){
                forBind = NEXT_ID;
                break;
            }else{
                if(s.equals(NEXT_ID)){
                    continue;
                }
                
                if(isFirst){
                    isFirst = false;
                    forBind = s;
                    continue;
                }

                forBind += ("," + s);
            }
            
        }
        return forBind;
    }
    
    
    public ArrayList<TariffPlan> list(int seller_id, String msisdn) throws SQLException{
        Connection conn = DB.getDBConnection();
        ArrayList<String> enableTariffPlanes = getTrplRules(seller_id, false);
        if(enableTariffPlanes.isEmpty()) {
            enableTariffPlanes = getTrplRules(seller_id, true);
        }
        
        String forBind = getTariffPlans(enableTariffPlanes, msisdn);
        
        PreparedStatement  prepre = 
                conn.prepareStatement("select "
                                        + "t.trpl_name name"
                                        + ", t.trpl_id id "
                                    + "from tariff_plan t"
                                        + ", tariff_plan_history th "
                                    + "where "
                                        + "sysdate between th.stime and th.etime "
                                        + "and smst_id = 1 "
                                        + "and th.trpl_id = t.trpl_id "
                                        + "and t.tpt_id = 1 "
                                        + "and t.trpl_id in("+ forBind +")");

        ArrayList<TariffPlan> tariffPlans = new ArrayList<TariffPlan>();
        
        try{
            ResultSet rs = prepre.executeQuery();
            try{
                while(rs.next()){
                    TariffPlan tariffPlan =  new TariffPlan();
                    tariffPlan.setId(rs.getInt("id"));
                    tariffPlan.setName(rs.getString("name"));
                    tariffPlans.add(tariffPlan);
                }
            }finally{
                rs.close();
            }
        }finally{
            prepre.close();
        }
        return tariffPlans;
    }
    
    
    
    public ArrayList<String> getTrplRules(int seller_id, boolean getNull) throws SQLException{
        
        Connection conn = DB.getDBConnection();
                
        String sql = null;
        
        if(getNull)
             sql = SQL.GET_TARIFF_PLANS_RULE_WHEN_NULL;
        else
             sql = SQL.GET_TARIFF_PLANS_RULE_WHEN_NOT_NULL;
        
        PreparedStatement  prepre = conn.prepareStatement(sql);
        ArrayList<String> trpls = new ArrayList<String>();

        try{
            if(!getNull)
                prepre.setInt(1, seller_id);

            ResultSet rs = prepre.executeQuery();
            try{
                while(rs.next()){    
                    for(String trpl : rs.getString("trpl").split(",")){
                        trpls.add(trpl);
                    }
                }
            }finally{
                rs.close();
            }
        }finally{
            prepre.close();
        }
        
        return trpls;
        
    }
    
    
    
    
}
