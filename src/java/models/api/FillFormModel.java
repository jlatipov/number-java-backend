/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javassist.compiler.ast.Stmnt;
import oracle.jdbc.OracleTypes;
import utils.Const;
import utils.DB;
import utils.Helper;
import utils.HelperModels;
import utils.SQL;

/**
 *
 * @author jamshed.latipov
 */
public class FillFormModel implements impl.implFillFormModel {
    public final String CAN_NOT_UPDATE_IMAGE = "Can not upload image to db";
    public final String BP_ORDER_SUCCESS = "OK";
    
    private InputStream front;
    private InputStream back;
    
    public static final int CANNOT_FIND_DEALER = -1;
    public static final String CANNOT_FIND_DEALER_MSG = "Cannot find dealer or dealer has not access";
    
    private String phoneNumber;
    private String strName;
    private String strSecondName;
    private String strLastName;
    private String strIcc;
    private String txtDateBirth;
    private String txtPassportWhen;
    private String txtCountry;
    private String txtRegion;
    private int gender;
    private String txtDistrict;
    private String txtCity;
    private String txtJamoat;
    private String txtVillage;
    private String txtStreet;
    private String txtHouse;
    private String txtPassportNumber;
    private String txtPassportPlace;
    private int tariffPlanId;
    private String txtPassportSerias;
    private int dealerId = 0;
    private int sellerId = 0;
    private int regType = 0;
    
    private String clientIp;    
    private int passType;
    private String email;


    
    // chi gap register only
    private String contactNumber;

    
    public static final int NORMAL_REG = 1;
    public static final int CHI_GAP_REG = 2;
    
    public FillFormModel(){
    }
    
    public int insert() throws SQLException, IOException, ParseException{
        Connection conn =  DB.getDBConnection();
        conn.setAutoCommit(false);
        
        String sql = SQL.INSERT_ORDER; 
        

        CallableStatement  stmt = conn.prepareCall(sql);
        MSISDN msisdn = new MSISDN();
        HelperModels.DealerAndIcc dealerAndIcc;
        
        
        if(this.getRegType() == NORMAL_REG){
             dealerAndIcc = msisdn.getDealer(getStrIcc(), getPhoneNumber());
        }else{
            dealerAndIcc = new HelperModels.DealerAndIcc();
            dealerAndIcc.setState(Const.OK);
            dealerAndIcc.setDealer(SQL.DEALER_ID_FOR_CHI_GAP_MSISDN_SELECT);
        
        }
        
        try{
            if(dealerAndIcc.getState() == Const.DB_CANNOT_FIND_RECORD){
                return Const.DB_CANNOT_FIND_RECORD;
            }else{
                stmt.setString(1, getStrName());
                stmt.setString(2, getStrSecondName());
                stmt.setString(3, getStrLastName());
                stmt.setString(4, dealerAndIcc.getIcc());
                stmt.setInt(5, getGender());
                stmt.setString(6, getTxtDateBirth());
                stmt.setString(7, getPhoneNumber());
                stmt.setString(8, getTxtCity());
                stmt.setString(9, getTxtCountry());
                stmt.setString(10, getTxtJamoat());
                stmt.setString(11, getTxtVillage());
                stmt.setString(12, getTxtDistrict());
                stmt.setString(13, getTxtRegion());
                stmt.setString(14, getTxtStreet());
                stmt.setString(15, getTxtHouse());
                stmt.setString(16, getTxtPassportNumber());
                stmt.setString(17, getTxtPassportSerias());
                stmt.setString(18, getTxtPassportWhen());
                stmt.setString(19, getTxtPassportPlace());
                stmt.setInt(20, dealerAndIcc.getDealer());
                stmt.setInt(21, getSellerId());
                stmt.setInt(22, getTariffPlanId());                
                stmt.setString(23, getContactNumber());
                stmt.setInt(24, getRegType());
                stmt.setString(25, getClientIp());
                stmt.setInt(26, getPassType());
                stmt.setString(27, getEmail());

                stmt.registerOutParameter(28, OracleTypes.NUMBER);

                stmt.execute();
                
                
                conn.commit();

                return stmt.getInt(28);

            }
        }finally{
            stmt.close();
        }
    }
    
    public String bpRegister(int order_id) throws SQLException{
        
        Connection conn =  DB.getDBConnection();
        conn.setAutoCommit(false);


        String sqlRegisterBP = SQL.BP_REGISTER;
        CallableStatement cstmt = conn.prepareCall(sqlRegisterBP);
        String result = null;
        try{
            cstmt.setInt(1, order_id);
            cstmt.registerOutParameter(2, OracleTypes.INTEGER);
            cstmt.registerOutParameter(3, OracleTypes.INTEGER);
            cstmt.registerOutParameter(4, OracleTypes.VARCHAR);
            cstmt.execute();

            int code = cstmt.getInt(3);


            if(code == 0){
                System.out.println("success");
                conn.commit();
                result = BP_ORDER_SUCCESS;
            }else{
                conn.rollback();
                System.out.println(cstmt.getString(4));
                result = cstmt.getString(4);
                changeState(order_id);
            }
        }finally{
            cstmt.close();
        }
        return result;
    }
    
    public boolean changeState(int id){
        try{
            Connection conn =  DB.getDBConnection();
            conn.setAutoCommit(false);

            String sql = SQL.CHANGE_ORDER_STATUS;
            
            CallableStatement  stmt = conn.prepareCall(sql);
            try{
                stmt.setInt(1, id);
                stmt.execute();
                conn.commit();
            }catch(Exception ex){
                System.out.println(ex);
                return false;
            }finally{
                stmt.close();
            }
            return true;
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
    
    
    public boolean uploadImage(String type, int orderId){
        try{
            Connection conn =  DB.getDBConnection();
            conn.setAutoCommit(false);

            InputStream img;
            String sql = "";

            if(type.equals(front)){
                img = this.getFront();
                sql = SQL.UPDATE_IMAGE_FRONT;
            }else{
                img = this.getBack();
                sql = SQL.UPDATE_IMAGE_BACK;
            }
            
            CallableStatement  stmt = conn.prepareCall(sql);
            try{
                stmt.setBinaryStream(1, img, 100000);
                stmt.setInt(2, orderId);
                stmt.execute();
                conn.commit();
            }catch(Exception ex){
                System.out.println(ex);
                return false;
            }finally{
                stmt.close();
            }
            return true;
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
    
    
    /**
     * @return the front
     */
    public InputStream getFront() {
        return front;
    }

    /**
     * @param front the front to set
     */
    public void setFront(InputStream front) {
        this.front = front;
    }

    /**
     * @return the back
     */
    public InputStream getBack() {
        return back;
    }

    /**
     * @param back the back to set
     */
    public void setBack(InputStream back) {
        this.back = back;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the strName
     */
    public String getStrName() {
        return strName;
    }
    
    

    /**
     * @param strName the strName to set
     */
    public void setStrName(String strName) {
        this.strName = strName;
    }

    /**
     * @return the strSecondName
     */
    public String getStrSecondName() {
        return strSecondName;
    }

    /**
     * @param strSecondName the strSecondName to set
     */
    public void setStrSecondName(String strSecondName) {
        this.strSecondName = strSecondName;
    }

    /**
     * @return the strLastName
     */
    public String getStrLastName() {
        return strLastName;
    }

    /**
     * @param strLastName the strLastName to set
     */
    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    /**
     * @return the strIcc
     */
    public String getStrIcc() {
        return strIcc;
    }

    /**
     * @param strIcc the strIcc to set
     */
    public void setStrIcc(String strIcc) {
        this.strIcc = strIcc;
    }

    /**
     * @return the txtDateBirth
     */
    public String getTxtDateBirth() {
        return txtDateBirth;
    }

    /**
     * @param txtDateBirth the txtDateBirth to set
     */
    public void setTxtDateBirth(String txtDateBirth) {
        this.txtDateBirth = txtDateBirth;
    }

    /**
     * @return the txtPassportGet
     */
    public String getTxtPassportWhen() {
        return txtPassportWhen;
    }

    /**
     * @param txtPassportGet the txtPassportGet to set
     */
    public void setTxtPassportGet(String txtPassportGet) {
        this.txtPassportWhen = txtPassportGet;
    }

    /**
     * @return the txtCountry
     */
    public String getTxtCountry() {
        return txtCountry;
    }

    /**
     * @param txtCountry the txtCountry to set
     */
    public void setTxtCountry(String txtCountry) {
        this.txtCountry = txtCountry;
    }

    /**
     * @return the txtRegion
     */
    public String getTxtRegion() {
        return txtRegion;
    }

    /**
     * @param txtRegion the txtRegion to set
     */
    public void setTxtRegion(String txtRegion) {
        this.txtRegion = txtRegion;
    }

    /**
     * @return the txtDistrict
     */
    public String getTxtDistrict() {
        return txtDistrict;
    }

    /**
     * @param txtDistrict the txtDistrict to set
     */
    public void setTxtDistrict(String txtDistrict) {
        this.txtDistrict = txtDistrict;
    }

    /**
     * @return the txtCity
     */
    public String getTxtCity() {
        return txtCity;
    }

    /**
     * @param txtCity the txtCity to set
     */
    public void setTxtCity(String txtCity) {
        this.txtCity = txtCity;
    }

    /**
     * @return the txtJamoat
     */
    public String getTxtJamoat() {
        return txtJamoat;
    }

    /**
     * @param txtJamoat the txtJamoat to set
     */
    public void setTxtJamoat(String txtJamoat) {
        this.txtJamoat = txtJamoat;
    }

    /**
     * @return the txtVillage
     */
    public String getTxtVillage() {
        return txtVillage;
    }

    /**
     * @param txtVillage the txtVillage to set
     */
    public void setTxtVillage(String txtVillage) {
        this.txtVillage = txtVillage;
    }

    /**
     * @return the txtStreet
     */
    public String getTxtStreet() {
        return txtStreet;
    }

    /**
     * @param txtStreet the txtStreet to set
     */
    public void setTxtStreet(String txtStreet) {
        this.txtStreet = txtStreet;
    }

    /**
     * @return the txtHouse
     */
    public String getTxtHouse() {
        return txtHouse;
    }

    /**
     * @param txtHouse the txtHouse to set
     */
    public void setTxtHouse(String txtHouse) {
        this.txtHouse = txtHouse;
    }

    /**
     * @return the txtNumber
     */
    public String getTxtPassportNumber() {
        return txtPassportNumber;
    }

    /**
     * @param txtNumber the txtNumber to set
     */
    public void setTxtPassportNumber(String txtNumber) {
        this.txtPassportNumber = txtNumber;
    }

    /**
     * @return the txtPassportPlace
     */
    public String getTxtPassportPlace() {
        return txtPassportPlace;
    }

    /**
     * @param txtPassportPlace the txtPassportPlace to set
     */
    public void setTxtPassportPlace(String txtPassportPlace) {
        this.txtPassportPlace = txtPassportPlace;
    }

    /**
     * @return the tariffPlanId
     */
    public int getTariffPlanId() {
        return tariffPlanId;
    }

    /**
     * @param tariffPlanId the tariffPlanId to set
     */
    public void setTariffPlanId(int tariffPlanId) {
        this.tariffPlanId = tariffPlanId;
    }

    /**
     * @return the txtPassportSerias
     */
    public String getTxtPassportSerias() {
        return txtPassportSerias;
    }

    /**
     * @param txtPassportSerias the txtPassportSerias to set
     */
    public void setTxtPassportSerias(String txtPassportSerias) {
        this.txtPassportSerias = txtPassportSerias;
    }

    /**
     * @return the gender
     */
    public int getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    /**
     * @return the dealerId
     */
    public int getDealerId() {
        return dealerId;
    }

    /**
     * @param dealerId the dealerId to set
     */
    public void setDealerId(int dealerId) {
        this.dealerId = dealerId;
    }

    /**
     * @return the sellerId
     */
    public int getSellerId() {
        return sellerId;
    }

    /**
     * @param sellerId the sellerId to set
     */
    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * @return the contactNumber
     */
    public String getContactNumber() {
        if(this.contactNumber == null){
            return "";
        }
        
        return this.contactNumber;
        
                
    }

    /**
     * @param contactNumber the contactNumber to set
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     * @return the regType
     */
    public int getRegType() {
        return regType;
    }

    /**
     * @param regType the regType to set
     */
    public void setRegType(int regType) {
        this.regType = regType;
    }

    /**
     * @return the clientIp
     */
    public String getClientIp() {
        return clientIp;
    }

    /**
     * @param clientIp the clientIp to set
     */
    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    /**
     * @return the passType
     */
    public int getPassType() {
        return passType;
    }

    /**
     * @param passType the passType to set
     */
    public void setPassType(int passType) {
        this.passType = passType;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        if(this.contactNumber == null){
            return "";
        }
        
        return this.email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
   
}
