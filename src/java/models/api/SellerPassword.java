/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utils.Const;
import utils.DB;
import utils.Hash;
import utils.SQL;

/**
 *
 * @author Jamshed.Latipov
 */



public class SellerPassword {
    
    public static final int PASSWORD_CONDIDATE_CONFIRMED = 1;
    public static final int  PASSWORD_CONDIDATE_NOT_CONFIRMED = 2;
    public static final int  PASSWORD_CONDIDATE_NOT_EQUAL = 3;
    
    public static final int PASSWORD_IN_USE = 4;
    public static final int PASSWORD_FREE = 5;
    public static final int PASSWORD_NOT_EQUAL = 6;
    
    public static final int OK = 0;
    
    
    
    
    
    
    private int pswdId;
    private String sellerPswd;
    private String stime;
    private String etime;
    private int status;
    private int sellerId;
    private String sellerPswdCondidate;
    private int sellerPswdCondidateStatus;
    
    private Connection dbConnection;
    private PreparedStatement pstmt;
    private DB db = new DB();
    
    public boolean checkIsPasswordsEquals(){
        return this.getSellerPswd().equals(this.getSellerPswdCondidate());
    }
    
    public int checkCondidateIsConfirmed(){
        if(checkIsPasswordsEquals()){
            if(this.getSellerPswdCondidateStatus() == PASSWORD_CONDIDATE_CONFIRMED){
                return OK;
            }
        }
        return PASSWORD_CONDIDATE_NOT_CONFIRMED;
    }
    
    public int checkCondidateIsNotConfirmed(){
        if(!checkIsPasswordsEquals()){
            if(this.getSellerPswdCondidateStatus() == PASSWORD_CONDIDATE_NOT_CONFIRMED){
                return OK;
            }
        }
        return PASSWORD_CONDIDATE_CONFIRMED;
    }
    
    
    public int authUsingCondidatePassword(String pincode){
        if(pincode.equals(this.getSellerPswdCondidate())){
            confirmeSellerPassword();
        }else{
            return PASSWORD_CONDIDATE_NOT_EQUAL;
        }
        return OK;
    
    }
    
    public int authUsingCurrentPassword(String pincode){
        if(pincode.equals(this.getSellerPswd())){
            return OK;
        }
        return PASSWORD_CONDIDATE_NOT_EQUAL;
    }
    
    
    
    
    public void confirmeSellerPassword(){
        this.setSellerPswd(this.getSellerPswdCondidate());                        
        this.setStatus(Const.SELLER_PASSWORD_ALREDY_IN_USE);
        this.setSellerPswdCondidateStatus(Const.SELLER_PASSWORD_WAS_CONFIRMED);
        this.update(this.getPswdId());
    }
    
    
    public void insert() throws SQLException{
        String sql = SQL.INSERT_SELLER_PASSWORD;
        dbConnection = db.getDBConnection();
        pstmt = dbConnection.prepareStatement(sql);
        
        pstmt.setString(1, this.getSellerPswd());
        pstmt.setInt(2, this.getStatus());
        pstmt.setInt(3, this.getSellerId());
        pstmt.setString(4, this.getSellerPswdCondidate());
        pstmt.setInt(5, this.getSellerPswdCondidateStatus());
        
        pstmt.execute();
        dbConnection.commit();
    }

    public void update(int id){
        try{
            String sql = SQL.UPDATE_SELLER_PASSWORD;
            dbConnection = db.getDBConnection();
            // 1 | SELLER_PSWD_CONDIDAT
            // 2 | SELLER_PSWD_CONDIDAT_STATUS
            // 3 | seller_pswd
            // 4 | status
            System.out.println(this.getSellerPswdCondidate());
            pstmt = dbConnection.prepareStatement(sql);
            pstmt.setString(1, Hash.md5Custom(this.getSellerPswdCondidate()));
            pstmt.setInt(2, this.getSellerPswdCondidateStatus());
            pstmt.setString(3, Hash.md5Custom(this.getSellerPswd()));
            pstmt.setInt(4, this.getStatus());
            
            pstmt.setInt(5, id);

            pstmt.execute();
            dbConnection.commit();
        }catch(SQLException ex){
            System.out.println(ex );
        }
    }
    
    
    public boolean find(int seller_id) throws SQLException{
        String sql = SQL.GET_SELLER_PASSWORD_BY_SELLER_ID;
        
        dbConnection = db.getDBConnection();
        pstmt = dbConnection.prepareStatement(sql);
        try{
        
            pstmt.setInt(1, seller_id);
            ResultSet rs = pstmt.executeQuery();
            try{
                if(rs.next()){
                    this.setSellerId(rs.getInt("seller_id"));
                    this.setStime(rs.getString("stime"));
                    this.setEtime(rs.getString("etime"));
                    this.setPswdId(rs.getInt("pswd_id"));
                    this.setSellerPswd(rs.getString("seller_pswd"));
                    this.setStatus(rs.getInt("status"));
                    this.setSellerPswdCondidate(rs.getString("seller_pswd_condidat"));
                    this.setSellerPswdCondidateStatus(rs.getInt("seller_pswd_condidat_status"));
                    return true;
                }
            }finally{
                rs.close();
            }
        }finally{
            pstmt.close();
        }
        
        return false;
        
    }
    
    
    
    
    
    /**
     * @return the pswdId
     */
    public int getPswdId() {
        return pswdId;
    }

    /**
     * @return the sellerPswd
     */
    public String getSellerPswd() {
        return sellerPswd == null ? this.getSellerPswdCondidate() : this.sellerPswd;
    }

    /**
     * @return the stime
     */
    public String getStime() {
        return stime;
    }

    /**
     * @return the etime
     */
    public String getEtime() {
        return etime;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @return the sellerId
     */
    public int getSellerId() {
        return sellerId;
    }

    /**
     * @return the sellerPswdCondidate
     */
    public String getSellerPswdCondidate() {
        return sellerPswdCondidate;
    }

    /**
     * @return the sellerPswdCondidateStatus
     */
    public int getSellerPswdCondidateStatus() {
        return sellerPswdCondidateStatus;
    }

    /**
     * @param pswdId the pswdId to set
     */
    public void setPswdId(int pswdId) {
        this.pswdId = pswdId;
    }

    /**
     * @param sellerPswd the sellerPswd to set
     */
    public void setSellerPswd(String sellerPswd) {
        this.sellerPswd = sellerPswd;
    }

    /**
     * @param stime the stime to set
     */
    public void setStime(String stime) {
        this.stime = stime;
    }

    /**
     * @param etime the etime to set
     */
    public void setEtime(String etime) {
        this.etime = etime;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @param sellerId the sellerId to set
     */
    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * @param sellerPswdCondidate the sellerPswdCondidate to set
     */
    public void setSellerPswdCondidate(String sellerPswdCondidate) {
        this.sellerPswdCondidate = sellerPswdCondidate;
    }

    /**
     * @param sellerPswdCondidateStatus the sellerPswdCondidateStatus to set
     */
    public void setSellerPswdCondidateStatus(int sellerPswdCondidateStatus) {
        this.sellerPswdCondidateStatus = sellerPswdCondidateStatus;
    }
    
}
