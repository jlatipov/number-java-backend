<%-- 
    Document   : FillForm
    Created on : Nov 11, 2015, 14:36:18 AM
    Author     : Jamshed.Latipov
--%>
<%@page import="NumberRelation.TariffPlan"%>
<%@page import="NumberRelation.Number"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    int msisdnId = Integer.parseInt(request.getParameter("msisdn_id"));
    
%>
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="includes/headContaint.jsp" />
    </head>
    <body>
        <jsp:include page="includes/menu.jsp" />
        <% session.setAttribute("title", "Маълумот оиди муштари"); %>
        <div class = 'container'>
                <form action ="" method = 'post' id = 'fillFormId'>
                    <div class="form-group" >
                        <input name ='name' class="form-control input-lg" type = 'text' placeholder=" Ном">
                    </div>
                    <div class="form-group" >
                        <input name ='last_name' class="form-control input-lg" type = 'text' placeholder="Насаб">
                    </div>
                    <div class="form-group" >
                        <input name ='second_name' class="form-control input-lg" type = 'text' placeholder="Номи падар">
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon" id="basic-addon3">436010100</span>
                        <input name ='icc' class="form-control input-lg" class ='widthMedium flat' placeholder="ICC (6 раками охир)" >
                    </div>
                    
                    <div class="form-group">
                        <select name ='tariff_plan' class="form-control input-lg" class ='widthMedium flat' >
                            <% TariffPlan tf = new TariffPlan(); %>
                            <%= tf.generateSelectOptions("0")%>
                        </select>
                    </div>
                    <div class="form-group" >
                        
                        <div class="pull-left">
                            <input id='woman' name ='gender'  type = 'radio'/> 
                            <label for ='woman'>Зан</label>
                        </div>
                        
                        <div class="pull-right">
                            <input id ='man' name ='gender'  type = 'radio'/> 
                            <label for ='man'>Мард</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input name ='passport_info' class="form-control input-lg" class ='widthMedium flat' placeholder="Раками шиноснома">
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon" id="basic-addon3">ШКД</span>
                        <input name ='passport_get_place' class="form-control input-lg" class ='widthMedium flat' placeholder=" Додашуд">
                    </div>
                    
                    <div  class="form-group btn-group" style="width:100%" role="group">
                        <input onclick ="initAutocomplete(this)" data-autocomplete-url ='autocompleteInput' name ='regionText' class="form-control input-lg dropdown-toggle"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type = 'text' placeholder="Вилоят">
                        <input type ='hidden' name = 'region' id = 'region' class ="valueContainer">
                        <ul class="dropdown-menu autocomplete"></ul>
                    </div>
                        
                        
                    <div  class="form-group btn-group" style="width:100%" role="group">
                        <input onclick ="initAutocomplete(this)" data-autocomplete-url ='autocompleteInput' name ='cityText' class="form-control input-lg dropdown-toggle"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type = 'text' placeholder="Шахр">
                        <input type ='hidden' class ="valueContainer" name = 'city' id = 'city'>
                        <ul class="dropdown-menu autocomplete"></ul>
                    </div>
                    
                    <div  class="form-group btn-group" style="width:100%" role="group">
                        <input onclick ="initAutocomplete(this)" data-autocomplete-url ='autocompleteInput' name ='sectionText' class="form-control input-lg dropdown-toggle"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type = 'text' placeholder="Чамоат">
                        <input type ='hidden' name = 'section' class ="valueContainer" id = 'section'>
                        <ul class="dropdown-menu autocomplete"></ul>
                    </div>
                       
                    <div  class="form-group">
                        <input name ='street' class="input-lg col-lg-9 col-sm-9 col-md-9 col-xs-9" type = 'text' placeholder="Сурога">
                        <input name ='address' class="input-lg col-lg-3 col-sm-3 col-md-3 col-xs-3" type = 'text' placeholder="Хона">
                        <br>                        
                        <br>
                    </div>
                        
                    <div  class="form-group">
                        <fieldset>
                            <legend>Санаи тавалуд</legend>
                            <input data-lenght = '2' name ='born_day' class="input-lg col-lg-3 col-sm-3 col-md-3 col-xs-3" type = 'number' placeholder="Руз">
                            <input data-lenght = '2' name ='born_month' class="input-lg col-lg-3 col-sm-3 col-md-3 col-xs-3" type = 'number' placeholder="Мох">
                            <input data-lenght = '4' name ='born_year' class="input-lg col-lg-6 col-sm-6 col-md-6 col-xs-6" type = 'number' placeholder="Сол">
                        </fieldset>

                    </div>
                    
                    <div  class="form-group" style="margin-top: 70px;">
                        <input  class ='form-control input-lg pink' value = 'Бакайдгири' type = 'submit'>
                    </div>
                </form>
            </div>
            
        </div>
        
        <script src = '../static/script.js' ></script>
        <script src = '../static/fillForm.js' ></script>
        <script src = '../static/autocomplete.js' ></script>


    </body>
</html>